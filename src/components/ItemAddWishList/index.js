import React, {useState} from 'react';
import styled from 'styled-components';

const StyledItem = styled.div`
    position:absolute;
    bottom:10%;
    right:10%;
    display:flex;
    justify-content:center;
    place-items: center;
    width:35px;
    height: 35px;
    border-radius: 50px;
    background-color: #fff;
    cursor:pointer;
   svg{
        width:20px;
   }
`;

function ItemAddWishList() {

    const [isChange, setIsChange] = useState(false);

    const onClick = () =>{
      setIsChange(!isChange);
    };

    return(
        <StyledItem onClick={onClick}>
            {
                isChange?<svg xmlns="http://www.w3.org/2000/svg" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 471.701 471.701">
                    <path d="M433.601,67.001c-24.7-24.7-57.4-38.2-92.3-38.2s-67.7,13.6-92.4,38.3l-12.9,12.9l-13.1-13.1   c-24.7-24.7-57.6-38.4-92.5-38.4c-34.8,0-67.6,13.6-92.2,38.2c-24.7,24.7-38.3,57.5-38.2,92.4c0,34.9,13.7,67.6,38.4,92.3   l187.8,187.8c2.6,2.6,6.1,4,9.5,4c3.4,0,6.9-1.3,9.5-3.9l188.2-187.5c24.7-24.7,38.3-57.5,38.3-92.4" fill='#ff4242'/>
                </svg>
                    :
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                    <path d="M6.28 3c3.236.001 4.973 3.491 5.72 5.031.75-1.547 2.469-5.021 5.726-5.021 2.058 0 4.274 1.309 4.274 4.182 0 3.442-4.744 7.851-10 13-5.258-5.151-10-9.559-10-13 0-2.676 1.965-4.193 4.28-4.192zm.001-2c-3.183 0-6.281 2.187-6.281 6.192 0 4.661 5.57 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-4.011-3.097-6.182-6.274-6.182-2.204 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248z"/>
                </svg>
            }
        </StyledItem>
    )
}
export default ItemAddWishList;
