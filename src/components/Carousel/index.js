import React, {useState} from 'react';
import {Carousel} from 'react-bootstrap';
import _ from 'lodash';
import ic_product_default from '../../Assets/assets/ic_product_default.png';
import './style.scss';
import Image from "react-bootstrap/Image";

function CarouselS() {

    const listCarousel = [
        {
            img:ic_product_default,
        },
        {
            img:ic_product_default,
        },
        {
            img:ic_product_default,
        },
        {
            img:ic_product_default,
        },
        {
            img:ic_product_default,
        },
        {
            img:ic_product_default,
        },
    ];

    const [index, setIndex] = useState(0);
    const [direction, setDirection] = useState(null);

    const handleSelect = (selectedIndex, e) => {
        setIndex(selectedIndex);
        setDirection(e.direction);
    };

    return (
        <Carousel prevIcon={<i className="fa fa-chevron-left" aria-hidden="true"></i>} nextIcon={<i className="fa fa-chevron-right" aria-hidden="true"></i>} activeIndex={index} direction={direction} onSelect={handleSelect}>
            {
                _.map(listCarousel, (item, index)=>{
                    return(
                            <Carousel.Item key={index}>
                                <Image
                                    className="d-block w-100"
                                    src={item.img}
                                />
                                <Carousel.Caption>
                                </Carousel.Caption>
                            </Carousel.Item>
                        )
                })
            }

        </Carousel>
    );
}
export default CarouselS;
