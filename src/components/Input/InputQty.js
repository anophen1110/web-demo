import React, {useState} from 'react';
import {Input} from 'reactstrap';
import './style.scss';

export default function InputQty(props) {
    const [value, setValue] = useState(parseInt(props.quantity)||1);
    const step = parseInt(props.step) || 1;

    return <div className='d-inline-block mr-2'>
            <div className="wr">
                {!props.disabled===true && <span onClick={()=>setValue(value - step)}><i className="fa fa-minus" aria-hidden="true"></i></span>}
                <Input className="input-qty" max={999} min={0} type='number' onChange={e=>setValue(parseInt(e.target.value))} value={value} name={props.name || `qty`} {...props}/>
                {!props.disabled===true && <span onClick={()=>setValue(value + step)}><i className="fa fa-plus" aria-hidden="true"></i></span>}
            </div>
        </div>
}

