import React from 'react';
import {Input} from 'reactstrap';
import styled from 'styled-components';

const StyledInput = styled(Input)`
    padding:10px 5px;
    border-radius:4px;
    outline:none;
    margin-bottom:10px;
`;

export default function InputStyle(props) {
    return   <StyledInput  {...props} ></StyledInput>;
}

