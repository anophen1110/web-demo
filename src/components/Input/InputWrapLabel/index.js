import React, {useState} from 'react';
import {Input} from 'reactstrap';
import LabelS from "../../FormGroup/Label";
import './style.scss';

function InputWrapLabel(props) {
    const [count, setCount] = useState(props.maxlength||255);
    const onChange = e =>{
        setCount(props.maxlength -(e.target.value).length);
    };

    return(
        <div className="input-wrap-label">
            <LabelS className='title' text={props.title}/>
            <LabelS className='value' text={props.labelValue}/>
            <Input className={'input '+(props.classinput)}  maxLength={props.maxlength||255} onChange={e=>onChange(e)} type={props.type||'text'} {...props}/>
            {props.maxlength && <div className='count-character'>{count} characters left</div>}
        </div>
    )
}
export default InputWrapLabel;
