import React from 'react';
import { Label} from "reactstrap";
import styled from 'styled-components';

const StyledLabel = styled(Label)`
    color: rgb(166, 166, 166);
    font-size:12px
    font-weight:500;
    margin-bottom:0;
    text-transform: uppercase;
`;

function LabelS(props) {
    return  <StyledLabel {...props}>{props.text}</StyledLabel>;

}


export default LabelS;
