import styled from 'styled-components';

export const FormGroupS = styled.div`
    padding:10px 20px;
    border-radius: 4px;
`;
