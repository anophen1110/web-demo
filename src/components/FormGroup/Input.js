import React from 'react';
import {Input} from "reactstrap";
import styled from 'styled-components';

const StyledInput = styled(Input)`
    border:none;
    border-bottom: 1px solid #cdcdcd;
    outline:none;
    border-radius:0;
    font-size:16px;
    padding-left:0;
    &:focus{
        box-shadow:none;
    }
`;

function InputS(props) {
    return    <StyledInput {...props}/>;
    }

export default InputS;
