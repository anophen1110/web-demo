import React from 'react';
import {Col, Row, Image} from 'react-bootstrap';
import ButtonAddToCart from '../Button/ButtonAddToCart';
import {Link} from 'react-router-dom';
import _ from 'lodash';
import './style.scss';
import ic_add_to_wishlist from '../../Assets/assets/ic_add_to_wishlist.png';
import reactLocalStorage from "../../utils/LocalStorage";

function GridPrd(props) {

    const handleAddToCart = (item) =>{
        console.log(item);
    };

    return(
        <div className="grid-product">{ props && props.list &&
            <Row>
                {
                    _.map(props.list, (item, index)=>{
                    return <Col key={index} xs={6} md={6} className='p-0'>
                        <div className="wr-item">
                            <div className="item">
                                <div className="item-image">
                                    <Link to={`product-detail`}><Image src={item.image}/></Link>
                                    {/*<Image src={ic_add_to_wishlist}/>*/}
                                </div>
                                <div className="item-name"><Link to={`product-detail`}>{item.name}</Link></div>
                                <div className="price-box position-relative">
                                    <div className="item-price item-old-price">{item.oldPrice}</div>
                                    <div className="item-price item-new-price">{item.newPrice}</div>
                                    {item.qty !=0 && <ButtonAddToCart handleAddToCart={handleAddToCart} item={item}/>}
                                    {item.qty===0 && <div className='out-of-stock qty-0'>
                                        <div className="label">Qty</div>
                                        <div className="value">0</div>
                                    </div>}
                                    {/*{item.qty===0 && <div className='out-of-stock'>Out of Stock</div>}*/}
                                </div>
                                <div className="buy-it-now">Buy it now and <span className='fw500'>save 20%</span></div>
                            </div>
                        </div>
                    </Col>
                })

                }
            </Row>
        }
        </div>
    )
}
export default GridPrd;
