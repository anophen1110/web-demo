import React from 'react';
import {Col, Row, Image} from 'react-bootstrap';
import {Link} from 'react-router-dom';
import _ from 'lodash';
import './style.scss';
import ButtonAddToCart from "../Button/ButtonAddToCart";


function ListPrd(props) {
    const handleAddToCart= (item) =>{

    };
    return(
        <div className="list-product">{ props&& props.list &&
            <Row>
                {
                    _.map(props.list, (item, index)=>{
                    return <Col  key={index} xs={12} className='p-0' >
                                <div className="wr-item">
                                    <div className="item">
                                        <div className="item-image mr-3">
                                            <Link to={`product-detail`}><Image src={item.image}/></Link>
                                        </div>
                                        <div className="item-details">
                                            <div className="item-name"><Link to={`product-detail`}>{item.name}</Link></div>
                                            <div className="price-box position-relative">
                                                <div className="item-new-price item-price">{item.newPrice}</div>
                                                {item.qty !=0 && <ButtonAddToCart handleAddToCart={handleAddToCart} item={item}/>}
                                                {/*{item.qty===0 && <div className='out-of-stock'>Out of Stock</div>}*/}
                                                {item.qty===0 && <div className='out-of-stock qty-0'>
                                                    <div className="label">Qty</div>
                                                    <div className="value">0</div>
                                                </div>}
                                            </div>

                                            <div className="buy-it-now">Buy it now and <span className='fw500'>save 20%</span></div>
                                        </div>

                                    </div>

                                </div>
                            </Col>
                        })

                }
            </Row>
        }
        </div>
    )
}
export default ListPrd;
