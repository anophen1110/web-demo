import React from 'react';
import {Image} from 'react-bootstrap';
import cartWarning from '../../Assets/assets/cartWarning.png';
import cartInfo from '../../Assets/assets/cartInfo.png';
import './style.scss';

function CartAlert(props) {
    return(
        <div>
            {
                props.type === 'warning' &&
                    <div  className="cart-alert">
                        <div className="content warning">
                            This product is currently out of stock.
                        </div>
                        <Image src={cartWarning}/>
                    </div>
            }
            {
                props.type === 'info' &&
                <div  className="cart-alert">
                    <div className="content info">
                        This product is currently out of stock.
                    </div>
                    <Image src={cartInfo}/>
                </div>
            }
            {
                props.type === 'normal' &&
                <div  className="cart-alert">
                    <div className="content normal">
                        This product is currently out of stock.
                    </div>
                </div>
            }
        </div>
    )
}
export default CartAlert;
