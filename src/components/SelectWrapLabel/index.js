import React, {useState} from 'react';
import Select from 'react-select';
import LabelS from "../FormGroup/Label";
import './style.scss';

function SelectWrap(props) {
    const [required, setRequired] = useState(false);
    const onChange = (value) =>{
        if(props.required){
            setRequired(true);
        }
        props.onChange(value);
    }
    return(
        <div className="wr-select">
            <LabelS text={props.label}/>
            {
                props.required &&<Select
                    {...props}
                    className={"select "+ (required?'':'border-fc3636 ') +(props.classselect)}
                    onChange={(value)=>onChange(value)}
                />
            }
            {
                !props.required &&<Select
                    {...props}
                    className={"select " +(props.classselect)}
                    onChange={(value)=>onChange(value)}
                />
            }
        </div>
    )
}
export default SelectWrap;
