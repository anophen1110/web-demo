import Rate from 'rc-rate';
import 'rc-rate/assets/index.css';
import React from "react";
import './styled.scss';

export default function Rating(props) {
    return    <Rate count={5} defaultValue={2} allowHalf style={{ fontSize: 25 }} onChange={(value)=> console.log(value)} {...props}/>
}
