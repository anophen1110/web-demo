import React from 'react';
import {Spinner} from 'reactstrap';
import styled from 'styled-components';

const StyledSpinner = styled(Spinner)`
    width: 23px;
    height:23px;
    border:2px solid rgb( 151, 151, 151);
    border-right-color: rgb( 228, 228, 228);
`;

export default function SpinnerS(props) {
    return   <StyledSpinner {...props}/>
}

