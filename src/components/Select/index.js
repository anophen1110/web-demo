import React from 'react';
import Select from "react-select";
import styled from 'styled-components';

const StyledSelect = styled(Select)`
    min-width:150px;
    border:none;
    border-bottom: 1px solid #cdcdcd;
    outline:none;
    border-radius:0;
    font-size:16px;
    &:focus{
        box-shadow:none;
    }
    .css-yk16xz-control{
        border:none;
    }
`;

function SelectS(props) {
    return    <StyledSelect {...props}/>;
}

export default SelectS;
