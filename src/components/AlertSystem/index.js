import React from 'react';
import {toast, ToastContainer} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.min.css';

const toastConfig = {
    position: toast.POSITION.TOP_CENTER,
    hideProgressBar:true,
    autoClose:4000
};

export function AlertSystem(props) {

    if(props.alert){
        switch (props.alert.type) {
            case 'success':
                toast.success(props.alert.message, toastConfig);
                break;
            case 'error':
                toast.error(props.alert.message, toastConfig);
                break;
            default:
                break;
        }
    }
    return(
        <ToastContainer/>
    )
}
