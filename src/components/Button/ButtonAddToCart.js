import {Button} from "react-bootstrap";
import React, {useState} from "react";
import './style.scss';


export default function ButtonAddToCart(props) {
    const [isLoading, setIsLoading] = useState(false);
    const [isSuccess, setIsSuccess] = useState(false);

    const onClick = () =>{
        props.handleAddToCart(props.item);
        setIsLoading(true);
         setTimeout(()=>{
             setIsLoading(false);
            setIsSuccess(true);
            setTimeout(()=>{
                setIsSuccess(false);
            },2000);
        },2000);
    };
    return(
        <div className="item-bag">
            <Button onClick={onClick} variant="success" type="button">
                {
                    isLoading===false && isSuccess === false &&
                    <svg height="14" viewBox="0 0 11 14" width="11" xmlns="http://www.w3.org/2000/svg">
                        <path d="M7.99,2.65v2.12c0,0.15 -0.11,0.27 -0.25,0.27c-0.14,0 -0.25,-0.12 -0.25,-0.27v-0.75c-0.29,
                                            0.11 -0.5,0.4 -0.5,0.75c0,0.38 0.25,0.7 0.58,0.78c0.05,0.01 0.11,0.02 0.16,0.02c0.41,
                                            0 0.75,-0.36 0.75,-0.8v-1.59h1.47l1.05,9.82h-11l1.05,-9.82h1.97v-0.53c0,-1.46 1.11,
                                            -2.65 2.49,-2.65c1.37,0 2.49,1.19 2.49,2.65zM3.51,2.65v2.12c0,0.15 -0.11,0.27 -0.25,
                                            0.27c-0.14,0 -0.25,-0.12 -0.25,-0.27v-0.75c-0.29,0.11 -0.5,0.4 -0.5,0.75c0,0.38 0.25,0.7 0.58,
                                            0.78c0.05,0.01 0.11,0.02 0.16,0.02c0.41,0 0.75,-0.36 0.75,-0.8v-1.59h3.48v-0.53c0,-1.17 -0.89,
                                            -2.12 -1.99,-2.12c-1.1,0 -1.99,0.95 -1.99,2.12z" fill="#fff"></path>
                    </svg>
                }
                {
                    isLoading === true && <div>
                        <svg width="16px" height="16px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100"
                             preserveAspectRatio="xMidYMid" className="lds-rolling" >
                            <circle cx="50" cy="50" fill="none" stroke="rgba(100%,100%,100%,0.91)" stroke-width="10"
                                    r="35" stroke-dasharray="164.93361431346415 56.97787143782138"
                                    transform="rotate(287.729 50 50)">
                                    <animateTransform attributeName="transform" type="rotate" calcMode="linear"
                                                  values="0 50 50;360 50 50" keyTimes="0;1" dur="1s" begin="0s"
                                                  repeatCount="indefinite"></animateTransform>
                            </circle>
                        </svg>
                    </div>
                }
                {
                    isSuccess === true && <div>
                        <svg className="svg-tick-container" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 30 30"
                             width="16px" height="16px">
                            <polyline className="path-tick" points="7.75 16.5 14.13 22.88 24.22 9.15"
                                     fill="none" stroke="#fff" stroke-width="3"/>

                        </svg>
                    </div>
                }
            </Button>
        </div>
    )
}
