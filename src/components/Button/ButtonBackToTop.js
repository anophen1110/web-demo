import React from 'react';
import styled from 'styled-components';

const StyleBTT = styled.div`
    border-radius:200px;
    box-shadow: 0 0 0 2px #cdcdcd;
    width: 40px;
    height: 40px;
    display: flex;
    justify-content: center;
    place-items: center;
`;

function ButtonBackToTop() {
    return (
        <StyleBTT>
            <i className="fa fa-arrow-up" aria-hidden="true"></i>
        </StyleBTT>
    )
}
export default ButtonBackToTop;
