import React from 'react';
import {Button, Image} from 'react-bootstrap';
import ic_close from '../../Assets/assets/ic_close.png';

function ButtonClose(props) {
    return(
        <div className="item-close">
            <Button className="btn-close" {...props}><Image src={ic_close}/></Button>
        </div>
    )
}
export default ButtonClose;
