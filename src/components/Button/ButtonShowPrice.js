import {Button} from "react-bootstrap";
import React from "react";
import './style.scss';

function ButtonShowPrice(props) {
    return <Button onClick={props.showPrice} className="btn-show-price">Show Price</Button>;
}
export default ButtonShowPrice;
