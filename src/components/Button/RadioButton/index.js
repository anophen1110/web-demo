import React from 'react';
import {Input,Label } from 'reactstrap';
import './style.scss';
function RadioButton(props) {
    return(
        <div className="radio-btn">
            <Input type="radio" name={props.name} id={props.id} className="form-radio ml-auto" checked={props.checked} innerRef={props.innerRef} {...props}/>
        </div>
    )
}
export default RadioButton;

