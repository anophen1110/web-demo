import React from 'react';
import './style.scss';
function CheckboxButton(props) {
    return(
        <div className={"checkbox-btn "+(props.className)}>
            <input type="checkbox" name={props.name} id={props.id} className="form-radio ml-auto"
                   checked={props.checked} onChange={props.onChangeCheckbox} ref={props.innerRef}
            />
        </div>
    )
}
export default CheckboxButton;
