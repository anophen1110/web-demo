
export const Event = {
  User: {
    LOGIN: "login",
    LOGOUT: "logout"
  },

  Cart: {
    GET_CART_COUNT: "get_cart_count",
    ADD_CART: "add_cart",
    VALIDATE_LOCAL_ERROR: "localError",
    REFRESH_CART: "refresh_cart"
  },

  WishList: {
    ADD_ITEM: "add_item",
    REFRESH_WISH_LIST: "refresh_wish_list",
    ADDING_NOTE: "adding_note"
  },

  PushNotification: {
    GET_UNREAD_NOTI: "get_unread_notification"
  },

  Search: {
    UPDATE_SEARCH_RESULT_TITLE: "update_title",
    START_SEARCH: "start_search"
  },

  TabBadge: {
    WISHLIST_BADGE: "wishlist_badge",
    CART_BADGE: "cart_badge"
  },

  UpdateProductListTitle: {
    UPDATE_PRODUCT_LIST_TITLE: "updateProductTitle"
  },

  LookBook: {
    UPDATE_LOOK_BOOK_TITLE: "UPDATE_LOOK_BOOK_TITLE"
  },

  InfoPage: {
    UPDATE_INFO_PAGE_TITLE: "UPDATE_INFO_PAGE_TITLE"
  },

  Address: {
    BACK_ON_CREATE_ADDRESS: "back_on_create_address",
    BACK_ON_EDIT_ADDRESS: "back_on_edit_address",
    CREATE_NEW_ADDRESS: "create_new_address"
  },

  LayoutEvent: {
    LAYOUT_OPEN: "layoutOpen",
    LAYOUT_CLOSE: "layoutClose"
  },

  Account: {
    BACK_ON_EDIT_PROFILE: "back_on_edit_profile"
  },

  SelectedModule: {
    SELECTED_MODULE: "SELECTED_MODULE"
  },

  HomeScreenDidScroll: {
    HOME_SCREEN_DID_SCROLL: "HOME_SCREEN_DID_SCROLL"
  },

  CheckoutEvent: {
    CREATE_BILLING_ADDRESS: "create_billing_address",
    SHOW_ADDRESS_DETAIL: "SHOW_ADDRESS_DETAIL"
  },

  StoreLocation: {
    SHOW_DISTANCE: "showDistance"
  }
};
