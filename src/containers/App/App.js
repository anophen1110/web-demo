import React from 'react';
import {Container} from 'react-bootstrap';
import { BrowserRouter as Router,Switch, Route, Redirect} from 'react-router-dom';
import HomePage from '../HomePage/index';
import DevLogin from '../DevLogin/index';
import UserLogin from '../UserLogin/index';
import ListApp from '../ListApp/index';
import More from '../More/index';
import ProductList from '../ProductList/index';
import ProductDetail from '../ProductDetail/index';
import ShoppingCart from '../ShoppingCart/index';
import Register from '../Register/index';
import MyAccount from '../MyAccount/index';
import EditProfile from '../MyAccount/EditProfile';
import MyOrder from '../MyOrder/index';
import MyWishlist from '../MyWishlist/index';
import EstimateShippingAndTax from '../ShoppingCart/EstimateShippingAndTax';
import MainNav from '../../components/MainNav';
import PageNotFound from '../PageNotFound/index';
import {AlertSystem} from '../../components/AlertSystem';
import AddressBook from "../AddressBook";
import CreateAddress from "../AddressBook/CreateAddress";
import MyOrderDetail from "../MyOrder/order-detail";
import Checkout from '../Checkout/index';
import SearchProduct from "../SearchProduct";
import '../../Style/global.scss';
import './style.scss';

function App({
    alert
             }) {
    const isRedirect = true;
    // const alert={type:'error',message:'success'};
    const alertProps = {alert};
  return  (
    <div className='wr-app'>
        <div className="bg-fff">
            <Router>
                <AlertSystem {...alertProps}/>
                <Switch>
                    <Route exact  path="/" component={DevLogin}/>
                    <Route exact  path="/home" component={HomePage}/>
                    <Route exact  path={`/register`} component={Register}/>
                    <Route exact  path={`/my-profile`} component={MyAccount}/>
                    <Route exact  path={`/user-login`} component={UserLogin}/>
                    <Route exact  path={`/edit-profile`} component={EditProfile}/>
                    <Route exact  path={`/my-order`} component={MyOrder}/>
                    <Route exact  path={`/my-order-detail`} component={MyOrderDetail}/>
                    <Route exact  path={`/wishlist`} component={MyWishlist}/>
                    <Route exact  path={`/address-book/list`} component={AddressBook}/>
                    <Route exact  path={`/address-book/manage`} component={CreateAddress}/>
                    <Route exact  path={`/address-book/manage/id`} component={CreateAddress}/>
                    <Route exact  path={`/list-app`} component={ListApp}/>
                    <Route exact  path={`/shopping-cart`} component={ShoppingCart}/>
                    <Route exact  path={`/search`} component={SearchProduct}/>
                    <Route exact  path={`/shopping-cart/estimate-shipping-tax`} component={EstimateShippingAndTax}/>
                    <Route exact  path={`/more`} component={More}/>
                    <Route exact  path={`/product-list`} component={ProductList}/>
                    <Route exact  path={`/product-detail`} component={ProductDetail}/>
                    <Route exact  path={`/checkout`} component={Checkout}/>
                    <Route exact  path={``} component={PageNotFound}/>
                </Switch>
                <MainNav/>
            </Router>
        </div>
    </div>
  );
}

export default App;
