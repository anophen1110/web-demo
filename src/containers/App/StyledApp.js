import styled from 'styled-components';

export const StyledWrapper =  styled.div`
    background:#f9f9f9;
    min-height:100vh;
    padding-bottom:65px;
    font-family: "Open Sans";
`;
