import React,{useEffect, useState} from 'react';
import {Container, Image, Button} from 'react-bootstrap';
import { connect } from 'react-redux';
import useForm from 'react-hook-form' ;
import {Redirect, Link} from 'react-router-dom';
import PropTypes from 'prop-types';
import * as Constants from '../../Constants';
import {setGlobalAPI, mergeHeader} from '../../Services/callDataAPI';
import * as AppURL from "../../Services/urlAPI";
import * as Utils from '../../utils';
import {callApiACT} from '../../redux/callApiRDC';
import {reducerNameHOR} from '../../redux';
import { HeaderKey, TypeRequest } from "../../Constants";

import {FormGroupS} from '../../components/FormGroup/index';
import {StyledForm, SpanEye, SpanClose} from './StyledDevLogin';
import InputS from '../../components/FormGroup/Input';
import LabelS from '../../components/FormGroup/Label';
import SelectS from '../../components/Select';
import imgApp from '../../Assets/assets/JMangoLogo.png';
import ic_password from '../../Assets/assets/ic_password.png';
import ic_password_hide from '../../Assets/assets/ic_password_hide.png';
import ic_close from '../../Assets/assets/ic_close.png';
import './style.scss';
import '../../Style/global.scss';
import reactLocalStorage from "../../utils/LocalStorage";
import Form from "react-bootstrap/Form";
import {compose} from "redux";

function DevLogin(props) {
    const methods = useForm();
    methods.register({name:'environment'},{});
    const [isShowP, setIsShowP] = useState(true);
    const [isCloseP, setIsCloseP] = useState(false);
    const [selectedEnv, setSelectedEnv] = useState(null);
    const [isLoading, setIsLoading] = useState(false);
    const [redirect, setRedirect] = useState(false);
    const options = [
        {value: 'int', label:'int'},
        {value: 'uat', label:'uat'},
        {value: 'prod', label:'prod'}
    ];
    let user = Utils.getSafeValue(
        reactLocalStorage.getObject(Constants.KeyAsyncStore.userNameDev),
        "username",
        ""
    );

    const isShowPassword = () =>{
        setIsShowP(!isShowP);
    };
    const handleEnv = (value) =>{
        setSelectedEnv(value);
        methods.setValue('environment',value.value);
    };

    const onUserClickedLoginButton = async (data) => {
        setIsLoading(true);
        let serverType = AppURL.DEV_REGISTER_SERVER;
        let header = mergeHeader({
            [HeaderKey.JM360_ENV]: data.environment
        });
        try {
            setGlobalAPI(serverType, header);
            let dataEndPoint = await props.callApiACT({
                typeRequest: TypeRequest.GET,
                name: reducerNameHOR.getEndPoint
            });
            let endPoint = Utils.getSafeValue(dataEndPoint, "Endpoint", "");
            setGlobalAPI(endPoint, header);
            reactLocalStorage.setObject(Constants.KeyAsyncStore.baseUrl, endPoint);

            let dataLogin = await props.callApiACT({
                typeRequest: TypeRequest.POST,
                name: reducerNameHOR.loginUser,
                url: AppURL.loginUser,
                params: data
            });
            // save data login here!
            setNewHeader(dataLogin);
            await reactLocalStorage.setObject(
                Constants.KeyAsyncStore.userNameDev,
                {'username':data.username}
            );
            setRedirect(true);
        } catch (errorMessage) {
            console.log("errorMessage",errorMessage)
        }

        setIsLoading(false);
    };

    const setNewHeader = dataLogin => {
        let soKeypairVersion = Utils.getSafeValue(
            dataLogin,
            "soKeypairVersion",
            ""
        );
        let soKeypairData = Utils.getSafeValue(dataLogin, "soKeypairData", "");
        let soKeypairExpiration = Utils.getSafeValue(
            dataLogin,
            "soKeypairExpiration",
            ""
        );
        let newHeader = {
            [Constants.HeaderKey.JM360_SO_KP]: soKeypairData,
            [Constants.HeaderKey.JM360_SO_KP_EXP]: soKeypairExpiration,
            [Constants.HeaderKey.JM360_SO_KP_VER]: soKeypairVersion
        };
        setGlobalAPI(undefined, newHeader);
    };



    return user !== ""?<Redirect to={'/list-app'}/>:(
    <div className="wr-dev-login">
        {redirect===true&&<Redirect to={'/list-app'}/>}
        <Container>
            <div className="text-center pt-3">
                <Image src={imgApp}/>
                <p>Login to preview your JMango360 apps</p>
            </div>
        </Container>
        <Form className='bg-fff' onSubmit={methods.handleSubmit(onUserClickedLoginButton)}>
            <Container>
                <div className=" m-auto p-10px">
                    <FormGroupS>
                        <LabelS text="email"/>
                        <InputS type="text" name="username" placeholder="User Name" innerRef={methods.register}/>
                    </FormGroupS>
                    <FormGroupS className="position-relative">
                        <LabelS text="password"/>
                        <InputS type={isShowP?"password":"text"} onChange={(e)=>{if(e.target.value!=null){setIsCloseP(true)}}} name="password" placeholder="password" innerRef={methods.register}/>
                        {isCloseP?<SpanClose><Image src={ic_close} /></SpanClose>:""}
                        <SpanEye onClick={isShowPassword}><Image src={isShowP?ic_password:ic_password_hide} /></SpanEye>
                    </FormGroupS>
                    <FormGroupS>
                        <LabelS text="environment"/>
                        <SelectS
                            options={options}
                            onChange={value => handleEnv(value)}
                            value={selectedEnv}
                            name="environment"
                        />
                    </FormGroupS>
                    <div className="pr-3 pl-3">
                        <Button className="w-100 bg-373737" variant="secondary" type="submit">Sign in</Button>
                    </div>
                </div>
            </Container>
        </Form>
        <Container>
            <div className="m-auto">
                <div className="text-center">
                    <p className="mb-0 mt-3">Or sign in with:</p>
                    <Button className="btn-fb mr-3">Facebook</Button>
                    <Button className="btn-gg" >Google</Button>
                    <p>Build version 2.4 (31345)</p>
                </div>
            </div>
        </Container>
    </div>
);
}

DevLogin.propTypes = {
    callApiACT: PropTypes.func,
};

function mapStateToProps(state) {
    return{}
}

function mapDispatchToProps(dispatch) {
    return {
        callApiACT: params => dispatch(callApiACT(params)),
    };
}
const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps,
);
export default compose(
    withConnect
)(DevLogin);
