import styled from 'styled-components';
import {Form} from 'react-bootstrap';


export const StyledForm =  styled(Form)`
    background: #fff;
`;

export const SpanEye = styled.span`
    position:absolute;
    right: 15px;
    bottom:20px;
    cursor:pointer;
`;

export const SpanClose = styled.span`
    cursor:pointer;
    position:absolute;
    right: 45px;
    bottom:20px;
    border-radius: 100%;
    border:1px solid #cdcdcd;
    height:20px;
    width:20px;
    line-height:14px;
    text-align:center;
    img{
        width:10px;
        height:10px;
    }
`;
