import React from 'react';
import {Container, Image, Button, Col, Row, Form, Accordion, Card} from 'react-bootstrap';
import useForm from 'react-hook-form';
import {Input} from 'reactstrap';

import './style.scss';
import '../../Style/global.scss';
import '../../Style/accordion.scss';
import {FormGroupS} from '../../components/FormGroup';
import  SelectS from '../../components/FormGroup/Select';
import  LabelS from '../../components/FormGroup/Label';
import RadioButton from "../../components/Button/RadioButton";
import ButtonBack from "../../components/Button/ButtonBack";

function EstimateShippingAndTax() {


    const methods = useForm();
    const onSubmit = () =>{

    };

    return(
         <div className="wr-shopping-cart wr-estimate-shipping-tax">
            <div className="top-bar-title one-row-two-item p-10px">
                <ButtonBack/>
                <div>Estimate Shipping and Tax</div>
                <div>&nbsp;</div>
            </div>
            <Form onSubmit={methods.handleSubmit(onSubmit)}>
                <div className="bg-fff">
                    <Container>
                        <FormGroupS>
                            <LabelS text="Country*"/>
                            <SelectS
                                // options={options}
                                // onChange={value => handleEnv(value)}
                                // value={selectedEnv}
                                name="country"
                            />
                        </FormGroupS>
                        <FormGroupS>
                            <LabelS text="State"/>
                            <SelectS
                                // options={options}
                                // onChange={value => handleEnv(value)}
                                // value={selectedEnv}
                                name="state"
                            />
                        </FormGroupS>
                        <FormGroupS>
                            <LabelS text="Post Code*"/>
                            <SelectS
                                // options={options}
                                // onChange={value => handleEnv(value)}
                                // value={selectedEnv}
                                name="postcode"
                            />
                        </FormGroupS>
                        <FormGroupS>
                            <Button className="w-100" type="submit" variant="primary">Get a quote</Button>
                        </FormGroupS>
                    </Container>
                </div>
                <div className="fs-14 color-rgb134 text-center m-3">Please select a shipping method</div>
                <div className="bg-fff mt-3">
                    <Container>
                        <FormGroupS className="fs-12 gr-radio-label">
                            <div className="mr-auto">
                                <div>
                                    <b>Free Shipping</b>
                                </div>
                                Free Shipping
                            </div>
                            <RadioButton id="btn1" name='name'/>
                        </FormGroupS>
                    </Container>
                </div>
                <div className="bg-fff mt-3">
                    <Container>
                        <FormGroupS className="fs-12 gr-radio-label">
                            <div className="mr-auto">
                                <div>
                                    <b>Free Shipping</b>
                                </div>
                                Free Shipping
                            </div>
                            <RadioButton id="btn1" name='name'/>
                        </FormGroupS>
                    </Container>
                </div>
                <div className="bg-fff mt-3">
                    <Container>
                        <FormGroupS className="fs-12 gr-radio-label">
                            <div className="mr-auto">
                                <div>
                                    <b>Free Shipping</b>
                                </div>
                                Free Shipping
                            </div>
                            <RadioButton id="btn1" name='name'/>
                        </FormGroupS>
                    </Container>
                </div>
            </Form>
        </div>
    )
}

export default EstimateShippingAndTax;
