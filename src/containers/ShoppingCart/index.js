import React, {useEffect, useState} from 'react';
import {Container, Image, Button, Col, Row, Form, Accordion, Card} from 'react-bootstrap';
import useForm from 'react-hook-form';
import {Link} from 'react-router-dom';
import InputS from '../../components/Input';
import './style.scss';
import '../../Style/global.scss';
import '../../Style/accordion.scss';
import ButtonTrash from '../../components/Button/ButtonTrash';
import CartAlert from '../../components/CartAlert';
import ic_product_default from '../../Assets/assets/ic_product_default.png';
import ButtonClose from "../../components/Button/ButtonClose";
import SpinnerS from "../../components/Spinner";
import InputQty from "../../components/Input/InputQty";


function ShoppingCart() {

    const [toggleAddCoupon, setToggleAddCoupon] = useState('1');
    const [isCloseP, setIsCloseP] = useState(true);
    const [validating, setValidating] = useState(false);
    const methods = useForm();
    const onSubmit = () =>{

    };

    return(
        <div>
            <div className="wr-shopping-cart">
                <div className="top-bar-title">Shopping Cart</div>
                <Form onSubmit={methods.handleSubmit(onSubmit)}>
                    <Container>
                        <div className="items">
                            <div className="item p-15px">
                                <div className="stickyInfo">New</div>
                                <div className="item-inner">
                                    <div className="item-image"><Image src={ic_product_default}/></div>
                                    <div className="item-content">
                                        <div className="name">Lawson convertible cot to toddler bed</div>
                                        <div className="d-flex align-items-baseline">
                                            <InputQty type='number' step={1} quantity={1}/> x <span>&nbsp;$50</span>
                                        </div>
                                        <div className="item-sub">
                                            <span className="fs-12">Subtotal:</span><span className="fs-16 fw-500">$6262</span>
                                            <ButtonTrash/>
                                        </div>
                                        <div className="fs-12 color-rgb134">Free Shipping</div>
                                        {/*<div className="item-details">*/}
                                        {/*    <Accordion defaultActiveKey="0">*/}
                                        {/*        <Card>*/}
                                        {/*            <Accordion.Toggle as={Card.Header} eventKey="1">*/}
                                        {/*                View Details*/}
                                        {/*                <i className="fa fa-angle-down" aria-hidden="true"></i>*/}
                                        {/*            </Accordion.Toggle>*/}
                                        {/*            <Accordion.Collapse eventKey="1">*/}
                                        {/*                <Card.Body>Hello! I'm the body</Card.Body>*/}
                                        {/*            </Accordion.Collapse>*/}
                                        {/*        </Card>*/}
                                        {/*    </Accordion>*/}
                                        {/*</div>*/}
                                        {/*<CartAlert type="warning"/>*/}
                                    </div>
                                </div>
                            </div>
                            <div className="item p-15px">
                                <div className="stickyInfo">New</div>
                                <CartAlert type="normal"/>
                                <div className="item-inner">
                                    <div className="item-image"><Image src={ic_product_default}/></div>
                                    <div className="item-content">
                                        <div className="name">Lawson convertible cot to toddler bed</div>
                                        <div className="d-flex align-items-baseline">
                                            <InputQty type='number' step={1} quantity={1}/> x <span>&nbsp;$50</span>
                                        </div>
                                        <div className="item-sub">
                                            <span className="fs-12">Subtotal:</span><span className="fs-16 fw-500">$6262</span>
                                            <ButtonTrash/>
                                        </div>
                                        <div className="item-details">
                                            <Accordion defaultActiveKey="0">
                                                <Card>
                                                    <Accordion.Toggle as={Card.Header} eventKey="1">
                                                        View Details
                                                        <i className="fa fa-angle-down" aria-hidden="true"></i>
                                                    </Accordion.Toggle>
                                                    <Accordion.Collapse eventKey="1">
                                                        <Card.Body>
                                                            <div className="d-flex w-100">
                                                                <div className="label-left">Size</div>
                                                                <div className="value-right">M</div>
                                                            </div>
                                                        </Card.Body>
                                                    </Accordion.Collapse>
                                                </Card>
                                            </Accordion>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="item p-15px">
                                <div className="item-inner free-gift">
                                    <div className="item-image"><Image src={ic_product_default}/></div>
                                    <div className="item-content">
                                        <span className="bg-rgb425875 fw-500 color-fff fs-12 p-1 mb-2">FREE GIFT</span>
                                        <div className="name">Lawson convertible cot to toddler bed</div>
                                        <div className="d-flex align-items-baseline">
                                            <InputQty disabled type='number' step={1} quantity={1}/> x <span>&nbsp;$50</span>
                                        </div>
                                        <div className="item-details">
                                            <Accordion defaultActiveKey="0">
                                                <Card>
                                                    <Accordion.Toggle as={Card.Header} eventKey="1">
                                                        View Details
                                                        <i className="fa fa-angle-down" aria-hidden="true"></i>
                                                    </Accordion.Toggle>
                                                    <Accordion.Collapse eventKey="1">
                                                        <Card.Body>
                                                            Detail
                                                        </Card.Body>
                                                    </Accordion.Collapse>
                                                </Card>
                                            </Accordion>
                                        </div>
                                        <CartAlert type="info"/>
                                    </div>
                                </div>
                            </div>
                            <div className="item mb-0">
                                <Accordion defaultActiveKey="0">
                                    <Card>
                                        <Accordion.Toggle className="bg-fff" as={Card.Header} eventKey="1">
                                            <div className="label-left">Subtotal(5 items):</div>
                                            <div className="value-right fw-500">$ 1111</div>
                                        </Accordion.Toggle>
                                    </Card>
                                </Accordion>
                            </div>
                            <div className="item mb-0">
                                <Accordion defaultActiveKey="0">
                                    <Card>
                                        <Accordion.Toggle className="bg-fff" as={Card.Header} eventKey="1">
                                            <div className="label-left">Shipping:</div>
                                            <div className="value-right fw-500">$ 1111</div>
                                            <i className="fa fa-angle-down" aria-hidden="true"></i>
                                        </Accordion.Toggle>
                                        <Accordion.Collapse eventKey="1">
                                            <Card.Body>Hello! I'm the body</Card.Body>
                                        </Accordion.Collapse>
                                    </Card>
                                </Accordion>
                            </div>
                            <div className="item mb-0">
                                <Accordion defaultActiveKey="0">
                                    <Card>
                                        <Accordion.Toggle className="bg-fff" as={Card.Header} eventKey="1">
                                            <div className="label-left">Tax:</div>
                                            <div className="value-right fw-500">$ 1111</div>
                                            <i className="fa fa-angle-down" aria-hidden="true"></i>
                                        </Accordion.Toggle>
                                        <Accordion.Collapse eventKey="1">
                                            <Card.Body>
                                                <div className="d-flex w-100">
                                                    <div className="label-left">Alcohol Tax (2 items)</div>
                                                    <div className="value-right fw-500">$ 11.11</div>
                                                </div>
                                            </Card.Body>
                                        </Accordion.Collapse>
                                    </Card>
                                </Accordion>
                            </div>
                            <div className="item mb-0">
                                <Accordion defaultActiveKey="0">
                                    <Card>
                                        <Accordion.Toggle className="bg-fff" as={Card.Header} eventKey="1">
                                            <div className="label-left">Discount:</div>
                                            <div className="value-right fw-500">- $1111</div>
                                            <i className="fa fa-angle-down" aria-hidden="true"></i>
                                        </Accordion.Toggle>
                                        <Accordion.Collapse eventKey="1">
                                            <Card.Body>
                                                <div className="d-flex w-100">
                                                    40PERCENT
                                                    <ButtonTrash/>
                                                </div>
                                            </Card.Body>
                                        </Accordion.Collapse>
                                    </Card>
                                </Accordion>
                            </div>
                            <div className="item mb-0">
                                <Accordion defaultActiveKey="0">
                                    <Card>
                                        <Accordion.Toggle className="bg-fff" as={Card.Header} eventKey="1">
                                            <Link to={`/shopping-cart/estimate-shipping-tax`}><Button className="add-coupon" type="button">Estimate Shipping and Tax</Button></Link>
                                        </Accordion.Toggle>
                                    </Card>
                                </Accordion>
                            </div>
                            <div className="item mb-0">
                                <Accordion defaultActiveKey="0">
                                    <Card>
                                        <Accordion.Toggle className="bg-fff" as={Card.Header} eventKey="1">
                                            {toggleAddCoupon==='1' &&<Button className="add-coupon" onClick={()=> setToggleAddCoupon('2')} type="button">Add Coupon Code</Button>}
                                            {toggleAddCoupon==='2' && <div className="field-add-coupon">
                                                <InputS type="text" onChange={e => {
                                                    if (e.target.value != null) {
                                                        setIsCloseP(true)
                                                    }
                                                }} placeholder="Enter coupon code here"/>
                                                {isCloseP?!validating&&<ButtonClose />:""}
                                                {!validating&&<Button type='button' className='btn-cancel ml-2' onClick={() => setToggleAddCoupon('1')}>Cancel</Button>}
                                                {validating ===true &&<span className="text-nowrap"><SpinnerS animation="border" role="status"/>&nbsp;Validating Coupon</span>}
                                            </div>}
                                        </Accordion.Toggle>
                                    </Card>
                                </Accordion>
                            </div>
                        </div>
                    </Container>
                    <div className="bar-checkout">
                        <Container>
                            <Row>
                                <Col col={6}>
                                    <div className="label">Order Total:</div>
                                    <div className="value">$345</div>
                                </Col>
                                <Col col={6}>
                                    <Link to={'/checkout'}>
                                        <Button type="submit" className="fw-500 w-100">Checkout</Button>
                                    </Link>
                                </Col>
                            </Row>
                        </Container>
                    </div>
                </Form>
                {/*cart-unselected -->>*/}
                {/*<div className="wr-shopping-cart">*/}
                {/*    <div className="wr-cart-unselected">*/}
                {/*        <Image src={ic_cart_unselected3x}/>*/}
                {/*        <div className="text">Your Shopping Bag is Empty</div>*/}
                {/*    </div>*/}
                {/*</div>*/}
            </div>

        </div>
    )
}

export default ShoppingCart;
