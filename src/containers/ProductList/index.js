import React, {useState} from 'react';
import useForm from 'react-hook-form' ;
import {Container, Image, Modal, Button, Card, Accordion} from 'react-bootstrap';
import SelectS from '../../components/Select';
import GridPrd from '../../components/ListProduct/GridPrd';
import ListPrd from '../../components/ListProduct/ListPrd';
import SinglePrd from '../../components/ListProduct/SinglePrd';

import './style.scss';
import '../../Style/sidebarModal.scss';
import '../../Style/global.scss';
import ic_product_default3x from '../../Assets/assets/ic_product_default3x.png';
import ic_gridView from '../../Assets/assets/ic_gridView/ic_gridView.png';
import ic_listView from '../../Assets/assets/ic_listView/ic_listView.png';
import ic_singleView from '../../Assets/assets/ic_singleView/ic_singleView.png';
import ic_filter from '../../Assets/assets/ic_filter/ic_filter.png';
import {FormGroupS} from "../../components/FormGroup";
import RadioButton from "../../components/Button/RadioButton";
import CheckboxButton from "../../components/Button/CheckboxButton";
import ButtonClose from "../../components/Button/ButtonClose";

export const ListProduct =[
    {
        name:'name 1',
        image:ic_product_default3x,
        oldPrice:'20',
        newPrice:'15',
        qty:20
    },
    {
        name:'name 2',
        image:ic_product_default3x,
        oldPrice:'20',
        newPrice:'15',
        qty:0
    },
    {
        name:'name 3',
        image:ic_product_default3x,
        oldPrice:'20',
        newPrice:'15',
        qty:0
    },
    {
        name:'name 4',
        image:ic_product_default3x,
        oldPrice:'20',
        newPrice:'15',
        qty:20
    },
    {
        name:'name 5',
        image:ic_product_default3x,
        oldPrice:'20',
        newPrice:'15',
        qty:20
    },
    {
        name:'name 6',
        image:ic_product_default3x,
        oldPrice:'20',
        newPrice:'15',
        qty:20
    },
    {
        name:'name 7',
        image:ic_product_default3x,
        oldPrice:'20',
        newPrice:'15',
        qty:20
    },
    {
        name:'name 8',
        image:ic_product_default3x,
        oldPrice:'20',
        newPrice:'15',
        qty:20
    },
];

function ProductList() {

    const [selected, setSelected] = useState(null);
    const [typeDisplayView, setTypeDisplayView] = useState('1');
    const [openSideView, setOpenSideView] = useState(false);
    const [openSideFilter, setOpenSideFilter] = useState(false);

    const SortOption = [
        {value:'PA', label:'Position(Ascending)'},
        {value:'PD', label:'Position(Descending)'},
        {value:'NA', label:'Product Name (Ascending)'},
        {value:'ND', label:'Product Name (Descending)'},
        {value:'PL', label:'Price: Low to High'},
        {value:'PH', label:'Price: High to Low'},
        {value:'PTA', label:'Pattern (Ascending)'},
        {value:'PTD', label:'Pattern (Descending)'}
    ];

    const onChangeSort = (value) =>{
        setSelected(value);
    };
    const openSidebarView = () =>{
        setOpenSideView(!openSideView);
    };
    const openSidebarFilter = () =>{
        setOpenSideFilter(!openSideFilter);
    };

    return(
        <div className="wr-product-list">
            <div className="top-bar-title">Name List</div>
            <Container>
                <div className="bar-filter">
                    <div className="sort">
                        <SelectS
                            name="sort"
                            options={SortOption}
                            onChange={value=>onChangeSort(value)}
                            className="mr-auto select-sort"
                        />
                    </div>

                    <button className='ml-auto' onClick={openSidebarFilter}><Image src={ic_filter}/></button>
                    {typeDisplayView === '1' && <button onClick={openSidebarView}><Image src={ic_gridView}/></button>}
                    {typeDisplayView === '2' && <button onClick={openSidebarView}><Image src={ic_listView}/></button>}
                    {typeDisplayView === '3' && <button onClick={openSidebarView}><Image src={ic_singleView}/></button>}
                </div>
            </Container>
            <Container>
                <div className="list-items">
                    {typeDisplayView === '1' && <GridPrd list={ListProduct}/>}
                    {typeDisplayView === '2' && <ListPrd list={ListProduct}/>}
                    {typeDisplayView === '3' && <SinglePrd list={ListProduct}/>}
                </div>

            </Container>
            <Modal dialogClassName="side" show={openSideView} onHide={openSidebarView}>
                <Modal.Header className='side-header' closeButton>
                    <div className='title'>VIEW</div>
                </Modal.Header>
                <Modal.Body >
                    <Container>
                        <div className="item" onClick={()=>{setTypeDisplayView('1'); setOpenSideView(false);}}><Image src={ic_gridView}/> Grid View</div>
                        <div className="item" onClick={()=>{setTypeDisplayView('2'); setOpenSideView(false);}}><Image src={ic_listView}/> List View</div>
                        <div className="item" onClick={()=>{setTypeDisplayView('3'); setOpenSideView(false);}}><Image src={ic_singleView}/> Single View</div>
                    </Container>
                </Modal.Body>
            </Modal>
            <Modal dialogClassName="side filter" show={openSideFilter} onHide={openSidebarFilter}>
                <Modal.Header>
                    <div className='fs-16 fw-500'>Filter</div>
                    <Button className="ml-auto clear">Clear All</Button>
                </Modal.Header>
                <Modal.Body>
                    <Container>
                        <div className="label-left color-rgb134">Currently shopping by:</div>
                        <div className="one-row-two-item w-100 fs-14 ">
                            <div className="mr-auto fw-500">View Details</div>
                            <div className="color-a7a7a7 mr-2">Jurken</div>
                            <Button variant='' className='btn-remove'>
                                <svg height="16" viewBox="0 0 16 16" width="16" xmlns="http://www.w3.org/2000/svg"><path d="M13.65,2.35c1.56,1.56 2.35,3.45 2.35,5.65c0,2.21 -0.78,4.09 -2.35,5.65c-1.56,1.56 -3.45,2.35 -5.65,2.35c-2.21,0 -4.09,-0.78 -5.65,-2.35c-1.56,-1.56 -2.35,-3.45 -2.35,-5.65c0,-2.21 0.78,-4.09 2.35,-5.65c1.56,-1.56 3.45,-2.35 5.65,-2.35c2.21,0 4.09,0.78 5.65,2.35zM12,10.88l-2.88,-2.88l2.88,-2.88l-1.12,-1.12l-2.88,2.88l-2.88,-2.88l-1.12,1.12l2.88,2.88l-2.88,2.88l1.12,1.12l2.88,-2.88l2.88,2.88z" fill="#282828" opacity="0.302"></path></svg>
                            </Button>
                        </div>
                        <div className="one-row-two-item w-100 fs-14 ">
                            <div className="mr-auto fw-500">View Details</div>
                            <div className="color-a7a7a7 mr-2">Jurken</div>
                            <Button variant='' className='btn-remove'>
                                <svg height="16" viewBox="0 0 16 16" width="16" xmlns="http://www.w3.org/2000/svg"><path d="M13.65,2.35c1.56,1.56 2.35,3.45 2.35,5.65c0,2.21 -0.78,4.09 -2.35,5.65c-1.56,1.56 -3.45,2.35 -5.65,2.35c-2.21,0 -4.09,-0.78 -5.65,-2.35c-1.56,-1.56 -2.35,-3.45 -2.35,-5.65c0,-2.21 0.78,-4.09 2.35,-5.65c1.56,-1.56 3.45,-2.35 5.65,-2.35c2.21,0 4.09,0.78 5.65,2.35zM12,10.88l-2.88,-2.88l2.88,-2.88l-1.12,-1.12l-2.88,2.88l-2.88,-2.88l-1.12,1.12l2.88,2.88l-2.88,2.88l1.12,1.12l2.88,-2.88l2.88,2.88z" fill="#282828" opacity="0.302"></path></svg>
                            </Button>
                        </div>
                    </Container>
                    <Accordion className="mt-3" defaultActiveKey="0">
                        <Card>
                            <Accordion.Toggle as={Card.Header} eventKey="1">
                                View Details
                                <i className="fa fa-angle-down" aria-hidden="true"></i>
                            </Accordion.Toggle>
                            <Accordion.Collapse eventKey="1">
                                <Card.Body>
                                    <div className="one-row-two-item item ">
                                        <div className="">Size</div>
                                        <div className=""><CheckboxButton name="size"/></div>
                                    </div>
                                    <div className="one-row-two-item item ">
                                        <div className="">Size</div>
                                        <div className=""><CheckboxButton name="size"/></div>
                                    </div>
                                    <div className="one-row-two-item item ">
                                        <div className="">Size</div>
                                        <div className=""><CheckboxButton name="size"/></div>
                                    </div>
                                </Card.Body>
                            </Accordion.Collapse>
                        </Card>
                    </Accordion>
                    <Accordion className="mt-3" defaultActiveKey="0">
                        <Card>
                            <Accordion.Toggle as={Card.Header} eventKey="1">
                                View Details
                                <i className="fa fa-angle-down" aria-hidden="true"></i>
                            </Accordion.Toggle>
                            <Accordion.Collapse eventKey="1">
                                <Card.Body>
                                    <div className="one-row-two-item item ">
                                        <div className="">Size</div>
                                        <div className=""><CheckboxButton name="size"/></div>
                                    </div>
                                    <div className="one-row-two-item item ">
                                        <div className="">Size</div>
                                        <div className=""><CheckboxButton name="size"/></div>
                                    </div>
                                    <div className="one-row-two-item item ">
                                        <div className="">Size</div>
                                        <div className=""><CheckboxButton name="size"/></div>
                                    </div>
                                </Card.Body>
                            </Accordion.Collapse>
                        </Card>
                    </Accordion>
                    <Accordion className="mt-3" defaultActiveKey="0">
                        <Card>
                            <Accordion.Toggle as={Card.Header} eventKey="1">
                                View Details
                                <i className="fa fa-angle-down" aria-hidden="true"></i>
                            </Accordion.Toggle>
                            <Accordion.Collapse eventKey="1">
                                <Card.Body>
                                    <div className="one-row-two-item item ">
                                        <div className="">Size</div>
                                        <div className=""><CheckboxButton name="size"/></div>
                                    </div>
                                    <div className="one-row-two-item item ">
                                        <div className="">Size</div>
                                        <div className=""><CheckboxButton name="size"/></div>
                                    </div>
                                    <div className="one-row-two-item item ">
                                        <div className="">Size</div>
                                        <div className=""><CheckboxButton name="size"/></div>
                                    </div>
                                </Card.Body>
                            </Accordion.Collapse>
                        </Card>
                    </Accordion>
                </Modal.Body>
                <div className="wr-button">
                    <Button onClick={openSidebarFilter  }>Done</Button>
                </div>
            </Modal>
        </div>
    )
}
export default ProductList;
