import React from 'react';
import imgConnect from '../../Assets/assets/ic_connection/ic_connection@2x.png';
import {Image, Button} from "react-bootstrap";
import './style.scss';

function Disconnect() {
    return(
        <div className='wr-disconnect'>
            <Image className="img" src={imgConnect}/>
            <div>No internet connection.</div>
            <div>Please try again.</div>
            <Button variant="secondary">Retry</Button>
        </div>
    )
}
export default Disconnect;
