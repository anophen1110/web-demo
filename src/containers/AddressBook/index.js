import React, {useState} from 'react';
import {Link} from 'react-router-dom';
import './style.scss';
import {Button, Container, Modal, Card, Accordion} from "react-bootstrap";
import ButtonClose from '../../components/Button/ButtonClose';
import CheckboxButton from '../../components/Button/CheckboxButton/index';
import RadioButton from "../../components/Button/RadioButton";
import ButtonTrash from "../../components/Button/ButtonTrash";
import _ from 'lodash';

function AddressBook() {

    const [openSideFilter, setOpenSideFilter] = useState(false);
    const ListAddress = [
        {
            id:1,
            name:'Name name 1',
            address1:'address address1 1',
            address2:'address address1 2',
            address3:'address address1 3',
            mobile:'0987654321',
        },
        {
            id:2,
            name:'Name name 2',
            address1:'address address2 1',
            address2:'address address2 2',
            address3:'address address2 3',
            mobile:'0987654321',
        },
        {
            id:3,
            name:'Name name 1',
            address1:'address address3 1',
            address2:'address address3 2',
            address3:'address address3 3',
            mobile:'0987654321',
        },
    ];
    const openSidebarFilter = () =>{
        setOpenSideFilter(!openSideFilter);
    };

    return(
        <div className="wr-address-book">
            <div className="top-bar-title">
                <Container>
                    <div className="text-center position-relative">
                        Address Book
                        <Link to={`/address-book/manage`}>
                            <div className="btn-sort">
                                <Button onClick={openSidebarFilter}>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24"><path d="M24 10h-10v-10h-4v10h-10v4h10v10h4v-10h10z"/></svg>                                </Button>
                            </div>
                        </Link>
                    </div>
                </Container>
            </div>
            <div className="items">
                {
                    _.map(ListAddress,(item,index)=>{
                        return(
                            <div className="item">
                                <Container>
                                    <div className="inner-item">
                                        <Link to={`/address-book/manage/${item.id}`}>
                                            <div className="details">
                                                <div className="title">{item.name}</div>
                                                <div className="desc">{item.address1}</div>
                                                <div className="desc">{item.address2}</div>
                                                <div className="desc">{item.address3}</div>
                                                <div className="desc tel"><Link>{item.mobile}</Link></div>
                                            </div>
                                        </Link>
                                        <i className="fa fa-angle-right" aria-hidden="true"></i>
                                        <ButtonTrash/>
                                    </div>
                                </Container>
                            </div>
                        )
                    })
                }

            </div>

            <Modal dialogClassName="side filter" show={openSideFilter} onHide={openSidebarFilter}>
                <Modal.Header>
                    <Modal.Title>Sort & Filter</Modal.Title>
                    <Button className="ml-auto clear">Clear All</Button>
                </Modal.Header>
                <Modal.Body>

                    <Accordion className="mt-3" defaultActiveKey="0">
                        <Card>
                            <Accordion.Toggle as={Card.Header} eventKey="1">
                                Sort by:
                                <i className="fa fa-angle-down" aria-hidden="true"></i>
                            </Accordion.Toggle>
                            <Accordion.Collapse eventKey="0">
                                <Card.Body>
                                    <div className="d-flex mb-2 w-100">
                                        <div className="label-left">Size</div>
                                        <div className="value-right"><RadioButton name="sort"/></div>
                                    </div>
                                    <div className="d-flex mb-2 w-100">
                                        <div className="label-left">Color</div>
                                        <div className="value-right"><RadioButton name="sort"/></div>
                                    </div>
                                    <div className="d-flex mb-2 w-100">
                                        <div className="label-left">Category</div>
                                        <div className="value-right"><RadioButton name="sort"/></div>
                                    </div>
                                </Card.Body>
                            </Accordion.Collapse>
                        </Card>
                    </Accordion>
                    <Accordion defaultActiveKey="0">
                        <Card>
                            <Accordion.Toggle as={Card.Header} eventKey="1">
                                Filter by:
                                <i className="fa fa-angle-down" aria-hidden="true"></i>
                            </Accordion.Toggle>
                            <Accordion.Collapse eventKey="0">
                                <Card.Body>
                                    <div className="d-flex mb-2 w-100">
                                        <div className="label-left">Size</div>
                                        <div className="value-right"><RadioButton name="filter"/></div>
                                    </div>
                                    <div className="d-flex mb-2 w-100">
                                        <div className="label-left">Color</div>
                                        <div className="value-right"><RadioButton name="filter"/></div>
                                    </div>
                                    <div className="d-flex mb-2 w-100">
                                        <div className="label-left">Category</div>
                                        <div className="value-right"><RadioButton name="filter"/></div>
                                    </div>
                                </Card.Body>
                            </Accordion.Collapse>
                        </Card>
                    </Accordion>
                </Modal.Body>
                <Modal.Footer>
                    <Button className='w-100'>Done</Button>
                </Modal.Footer>
            </Modal>
        </div>
    )
}
export default AddressBook;
