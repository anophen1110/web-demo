import React from 'react';
import * as history from 'history';
import useForm from 'react-hook-form';
import {Button, Container, Form, Image,Modal} from "react-bootstrap";
import '../Register/style.scss';
import InputWrapLabel from "../../components/Input/InputWrapLabel";
import SelectWrap from "../../components/SelectWrapLabel";

function CreateAddress() {
    const methods = useForm();
    methods.register({name:'country'});
    const onSubmit = e =>{
        console.log(e);
        history.createBrowserHistory().goBack();
    };
    return(
        <div className="wr-register edit-profile">
            <div className="top-bar-title">Edit Address</div>
            <div className="text-alert-required">* Please fill in all required fields</div>
            <div className="wr-content">
                <Form onSubmit={methods.handleSubmit(onSubmit)}>
                    <div className="wr-inner-content">
                        <Container>
                            <div className='wr-input'>
                                <InputWrapLabel type='text' name='firstName' classinput="input-no-border" title='FIRST NAME*' innerRef={methods.register}/>
                            </div>
                            <div className='wr-input'>
                                <InputWrapLabel type='text' name='lastName' classinput="input-no-border" title='LAST NAME*' innerRef={methods.register}/>
                            </div>
                            <div className='wr-input'>
                                <InputWrapLabel type='mail' name='mail' classinput="input-no-border" title='Company (optional)' innerRef={methods.register}/>
                            </div>
                            <div className='wr-input'>
                                <SelectWrap
                                    name='country'
                                    classselect='input-no-border'
                                    label='country*'
                                />
                            </div>
                            <div className='wr-input'>
                                <InputWrapLabel type='text' name='mobile' classinput="input-no-border" title='State (optional)' innerRef={methods.register}/>
                            </div>
                            <div className='wr-input'>
                                <InputWrapLabel type='date' name='DoB' classinput="input-no-border" title='address' innerRef={methods.register}/>
                            </div>
                            <div className='wr-input'>
                                <InputWrapLabel type='text' name='NIN' classinput="input-no-border" title='zip code (optional)' innerRef={methods.register}/>
                            </div>
                            <div className='wr-input'>
                                <InputWrapLabel type='text' name='city' classinput="input-no-border" title='city*' innerRef={methods.register}/>
                            </div>
                            <div className='wr-input'>
                                <InputWrapLabel type='number' name='phone' classinput="input-no-border" title='phone*' innerRef={methods.register}/>
                            </div>
                        </Container>
                    </div>
                    <div className="wr-bar-submit">
                        <Container>
                            <div className="wr-btn-submit">
                                <Button type='submit'>Submit</Button>
                            </div>
                        </Container>
                    </div>
                </Form>
            </div>
        </div>
    )
}
export default CreateAddress;
