import React from 'react';
import {Link} from 'react-router-dom';
import {Container, Image} from 'react-bootstrap';
import './style.scss';
import ic_edit_profile from '../../Assets/assets/ic_edit_profile.png';
import ic_my_order from '../../Assets/assets/ic_my_order.png';
import ic_my_favourite from '../../Assets/assets/ic_my_favourite.png';
import ic_address_book from '../../Assets/assets/ic_address_book.png';
import ic_avatar from '../../Assets/assets/ic_avatar.png';

function MyAccount() {
    return(
        <div className='wr-my-account'>
            <div className="wr-header">
                <Container>
                    <div className="inner-header">
                        <div className="img">
                            <Image src={ic_avatar}/>
                        </div>
                        <div className="text-name">Name 1 name</div>
                        <div className="text-mail">mail1@gmail.com</div>
                    </div>
                </Container>
            </div>
            <div className="bg-fff mb-10px">
                <Container>
                    <Link to={`/edit-profile`}><div className="link">
                        <div className="img"><Image src={ic_edit_profile}/></div>Edit Profile<i className="fa fa-angle-right" aria-hidden="true"></i>
                    </div></Link>
                </Container>
            </div>
            <div className="bg-fff mb-10px">
                <Container>
                    <Link to={`/my-order`}><div className="link">
                        <div className="img"><Image src={ic_my_order}/></div>
                        My Orders
                        <i className="fa fa-angle-right" aria-hidden="true"></i>
                    </div></Link>
                    <Link to={`/my-wishlist`}><div className="link">
                        <div className="img"><Image src={ic_my_favourite}/></div>
                        My Wishlist
                        <i className="fa fa-angle-right" aria-hidden="true"></i>
                    </div></Link>
                    <Link to={`/address-book/list`}><div className="link">
                        <div className="img"><Image src={ic_address_book}/></div>
                        Address Book
                        <i className="fa fa-angle-right" aria-hidden="true"></i>
                    </div></Link>
                </Container>
            </div>
            <div className="bg-fff mb-10px">
                <Container>
                    <div className='wr-logout'>Log Out</div>
                </Container>
            </div>
            <Container>
                <div className="version">v 1.5.0 Build 34767</div>
            </Container>
        </div>
    )
}

export default MyAccount;
