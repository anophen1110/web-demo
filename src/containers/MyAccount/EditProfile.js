import React, {useState} from 'react';
import useForm from 'react-hook-form';
import {Button, Container, Form, Image,Modal} from "react-bootstrap";
import '../../Style/global.scss';
import '../Register/style.scss';
import InputWrapLabel from "../../components/Input/InputWrapLabel";
import CheckboxButton from "../../components/Button/CheckboxButton";
import ic_password from "../../Assets/assets/ic_password.png";
import ic_password_hide from "../../Assets/assets/ic_password_hide.png";
import {SpanEye} from "../DevLogin/StyledDevLogin";

function EditProfile() {
    const methods = useForm();
    const [isShowP, setIsShowP] = useState(true);
    const [isShowConfirmP, setIsShowConfirmP] = useState(true);
    const [gender, setGender] = useState(true);
    const [customerType, setCustomerType] = useState(true);
    const [showDisclaimer, setShowDisclaimer] = useState(false);
    const [showTerm, setShowTerm] = useState(false);

    const isShowPassword = () =>{
        setIsShowP(!isShowP);
    };

    const isShowConfirmPassword = () =>{
        setIsShowConfirmP(!isShowConfirmP);
    };

    return(
        <div className="wr-register edit-profile">
            <div className="top-bar-title">Edit Profile</div>
            <div className="text-alert-required">* Please fill in all required fields</div>
            <div className="wr-content">
                <Form>
                    <div className="wr-inner-content">
                        <Container>
                            <div className="row-gender">
                                <div className="label">Gender</div>
                                <label className="value ml-auto">
                                    <input type="radio" name='gender'/>
                                    <span className="checkmark">Unspecified</span>
                                </label>
                                <label className="value ml-2" >
                                    <input type="radio" name='gender'/>
                                    <span className="checkmark">Male</span>
                                </label>
                                <label className="value ml-2">
                                    <input type="radio" name='gender'/>
                                    <span className="checkmark">Female</span>
                                </label>
                            </div>
                            <div className='wr-input'>
                                <InputWrapLabel type='text' name='firstName' classinput="input-no-border" title='FIRST NAME*'/>
                            </div>
                            <div className='wr-input'>
                                <InputWrapLabel type='text' name='middleName' classinput="input-no-border" title='MIDDLE NAME'/>
                            </div>
                            <div className='wr-input'>
                                <InputWrapLabel type='text' name='lastName' classinput="input-no-border" title='LAST NAME*'/>
                            </div>
                            <div className='wr-input'>
                                <InputWrapLabel type='mail' name='mail' classinput="input-no-border" title='EMAIL ADDRESS*'/>
                            </div>
                            <div className='wr-input'>
                                <InputWrapLabel type='text' name='vatTax' classinput="input-no-border" title='VAT/Tax Number'/>
                            </div>
                        </Container>
                    </div>

                    <div className="wr-bar-submit">
                        <Container>
                            <div className="wr-btn-submit">
                                <Button type='submit'>Edit</Button>
                            </div>
                        </Container>
                    </div>
                </Form>
            </div>
        </div>
    )
}
export default EditProfile;
