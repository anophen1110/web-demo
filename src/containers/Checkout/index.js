import React, {useState, createRef} from 'react';
import {Stepper,Step,StepLabel, Typography} from '@material-ui/core';
import {Button, Container, Modal} from "react-bootstrap";
import {Link} from 'react-router-dom';
import './style.scss';
import BillingInformation from "./steppers/BillingInformation";
import ShippingAddress from "./steppers/ShippingAddress";
import ShippingMethod from "./steppers/ShippingMethod";
import PaymentMethod from "./steppers/PaymentMethod";
import OrderReview from "./steppers/OrderReview";



function Checkout() {
    const [exitCheckout, setExitCheckout] = useState(false);
    const [activeStep, setActiveStep] = useState(0);

    const getStepContent=(stepIndex)=> {
        switch (stepIndex) {
            case 0:
                return <BillingInformation
                    activeStep={activeStep}
                    setActiveStep={setActiveStep}
                    handleBack={handleBack}
                    handleNext={handleNext}
                    steps={steps}
                />;
            case 1:
                return <ShippingAddress
                    activeStep={activeStep}
                    setActiveStep={setActiveStep}
                    handleBack={handleBack}
                    handleNext={handleNext}
                    steps={steps}
                />;
            case 2:
                return <ShippingMethod
                    activeStep={activeStep}
                    setActiveStep={setActiveStep}
                    handleBack={handleBack}
                    handleNext={handleNext}
                    steps={steps}
                />;
            case 3:
                return <PaymentMethod
                    activeStep={activeStep}
                    setActiveStep={setActiveStep}
                    handleBack={handleBack}
                    handleNext={handleNext}
                    steps={steps}
                />;
            case 4:
                return <OrderReview
                    activeStep={activeStep}
                    setActiveStep={setActiveStep}
                    handleBack={handleBack}
                    handleNext={handleNext}
                    steps={steps}
                />;
            default:
                return 'Unknown stepIndex';
        }
    };

    const steps = ['Billing Information', 'Shipping Address', 'Shipping Method', 'Payment Method', 'Order Review'];

    const getStepTitle = (stepIndex)=> {
        switch (stepIndex) {
            case 0:
                return '1. Billing Information';
            case 1:
                return '2. Shipping Address';
            case 2:
                return '3. Shipping Method';
            case 3:
                return '4. Payment Method';
            case 4:
                return '5. Order Review';
            default:
                return 'Unknown stepIndex';
        }
    };

    const handleNext = (item) => {
        setActiveStep(prevActiveStep => prevActiveStep + 1);
        console.log(item);

    };

    const handleBack = () => {
        setActiveStep(prevActiveStep => prevActiveStep - 1);
    };

    const handleReset = () => {
        setActiveStep(0);
    };

    return (
        <div className='wr-checkout'>
            <div className="item">
                <div className="inner-item">
                    <Container>
                        <div className="one-row-two-item">
                            <div className="page-title fs-18 fw-500">{getStepTitle(activeStep)}</div>
                            <div onClick={()=>setExitCheckout(true)} className="cursor-pointer color-0076ff fw-500">Cancel Checkout</div>
                        </div>
                        <Modal dialogClassName={'wr-checkout'} size='xs' show={exitCheckout} onHide={()=>setExitCheckout(false)}>
                            <Modal.Body>
                                <div className="fs-16 text-center">
                                    Do you want to cancel the checkout and go back to shopping cart?
                                </div>
                                <div className="one-row-two-item">
                                    <Button variant='secondary' onClick={()=>setExitCheckout(false)}>No</Button>
                                    <Link to={'/shopping-cart'}><Button>Yes</Button></Link>
                                </div>
                            </Modal.Body>
                        </Modal>
                    </Container>
                    <Stepper activeStep={activeStep} alternativeLabel>
                        {steps.map((label,index) => (
                            <Step key={label}>
                                <StepLabel>{label}</StepLabel>
                            </Step>
                        ))}
                    </Stepper>
                </div>
            </div>
            <div className=''>{getStepContent(activeStep)}</div>
        </div>
    );
}
export default Checkout;
