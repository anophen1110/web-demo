import React, {useState} from 'react';
import './../style.scss';
import useForm from 'react-hook-form';
import {Button, Container, Form} from "react-bootstrap";
import {Link} from 'react-router-dom';
import RadioButton from "../../../components/Button/RadioButton";
import CheckboxButton from "../../../components/Button/CheckboxButton";
import Label from "reactstrap/es/Label";

function ShippingAddress(props) {
    const methods = useForm();

    return(
        <div className='step'>
            <div className="helper-text">
                Please select a shipping address
            </div>
            <Form>
                <div className="item">
                    <Container>
                        <div className="inner-item">
                            <div className="one-row-two-item">
                                <div>
                                    <div className="color-a7a7a7">Same as billing address</div>
                                </div>
                                <RadioButton name='billing'/>
                            </div>
                        </div>
                    </Container>
                </div>
                <div className="item">
                    <Container>
                        <div className="inner-item">
                            <div className="one-row-two-item">
                                <div>
                                    <div className="font-weight-bold">Name Name</div>
                                    <div className="color-a7a7a7">F542+96 La Veguada, Panama</div>
                                    <div className="color-a7a7a7">Calobre District</div>
                                    <div className="color-a7a7a7">Panama</div>
                                    <Link>0987654321</Link><br/>
                                    <Link to={`/address-book/manage/id`} className="color-0076ff cursor-pointer">Edit address details</Link>
                                </div>
                                <RadioButton name='billing'/>
                            </div>
                        </div>
                    </Container>
                </div>
                <div className="item">
                    <Container>
                        <div className="inner-item">
                            <div className="one-row-two-item">
                                <div>
                                    <div className="font-weight-bold">Name Name</div>
                                    <div className="color-a7a7a7">F542+96 La Veguada, Panama</div>
                                    <div className="color-a7a7a7">Calobre District</div>
                                    <div className="color-a7a7a7">Panama</div>
                                    <Link>0987654321</Link>
                                    <div className="color-0076ff cursor-pointer">Edit address details</div>
                                </div>
                                <RadioButton name='billing'/>
                            </div>
                        </div>
                    </Container>
                </div>
                <div className="item">
                    <Container>
                        <div className="inner-item">
                            <div className="one-row-two-item">
                                <div className="color-0076ff cursor-pointer">Use different billing address</div>
                            </div>
                        </div>
                    </Container>
                </div>
            </Form>
            <div className='wr-button'>
                <Container>
                    <div className="inner-wr-button one-row-two-item">
                        <Button
                            onClick={props.handleBack}
                            variant='secondary'
                            className=''
                        >
                            Previous
                        </Button>
                        <Button variant="primary" onClick={props.handleNext}>
                            Next
                        </Button>
                    </div>
                </Container>
            </div>
        </div>
    )
}
export default ShippingAddress;
