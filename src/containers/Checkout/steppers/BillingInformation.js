import React, {createRef, useEffect, useState} from 'react';
import './../style.scss';
import useForm from 'react-hook-form';
import {Button, Container, Form} from "react-bootstrap";
import {Link} from 'react-router-dom';
import RadioButton from "../../../components/Button/RadioButton";
import _ from 'lodash';
import CheckboxButton from "../../../components/Button/CheckboxButton";
import Label from "reactstrap/es/Label";

function ItemBilling(props) {

    const refStep = createRef();

    return(
        <div className="item" >
            <Container>
                <div className="inner-item">
                    <div className="one-row-two-item">
                        <div>
                            <div className="font-weight-bold">{props.item.name}</div>
                            <div className="color-a7a7a7">{props.item.address1}</div>
                            <div className="color-a7a7a7">{props.item.address2}</div>
                            <div className="color-a7a7a7">{props.item.address3}</div>
                            <Link to={''}>{props.item.mobile}</Link><br/>
                            <Link to={'/address-book/manage/id'} className="color-0076ff cursor-pointer">Edit address details</Link>
                        </div>
                        <RadioButton name='billing'
                                     value={props.item.id}
                                     onChange={(e)=>props.setDisableButton(!e.target.checked)}
                                     innerRef={props.methods.register}
                        />
                    </div>
                </div>
            </Container>
        </div>
    )
}

function BillingInformation(props) {
    const [itemEdit, setItemEdit] = useState(null);
    const [toggle, setToggle] = useState(true);
    const [disableButton, setDisableButton] = useState(true);
    const methods = useForm();
    const ListAddress = [
        {
            id:1,
            name:'Name name 1',
            address1:'address address1 1',
            address2:'address address1 2',
            address3:'address address1 3',
            mobile:'0987654321',
        },
        {
            id:2,
            name:'Name name 2',
            address1:'address address2 1',
            address2:'address address2 2',
            address3:'address address2 3',
            mobile:'0987654321',
        },
        {
            id:3,
            name:'Name name 3',
            address1:'address address3 1',
            address2:'address address3 2',
            address3:'address address3 3',
            mobile:'0987654321',
        },
    ];
    const onSubmit = e =>{
      props.handleNext(_.find(ListAddress,(o)=>{return o.id==e.billing}));
    };

    return(
        <div className='step'>
            <Form onSubmit={methods.handleSubmit(onSubmit)}>
            <div className="helper-text">
                Please select a billing address
            </div>
            {
                _.map(ListAddress, (item, index)=>{
                    return(
                        <ItemBilling methods={methods} key={index} item={item} setDisableButton={setDisableButton}/>
                    )
                })
            }
            <div className="item">
                <Container>
                    <div className="inner-item">
                        <div className="one-row-two-item">
                            <Link to={'/address-book/manage'} className="color-0076ff cursor-pointer">Use different address</Link>
                        </div>
                    </div>
                </Container>
            </div>
            <div className='wr-button'>
                <Container>
                    <div className="inner-wr-button one-row-two-item">
                        <>
                            <Button
                                disabled={true}
                                variant='secondary'
                                className=''
                            >
                                Previous
                            </Button>
                            <Button
                                variant="primary"
                                // onClick={props.handleNext}
                                type='submit'
                                disabled={disableButton}
                            >
                                Next
                            </Button>
                        </>
                    </div>
                </Container>
            </div>
            </Form>
        </div>
    )
}
export default BillingInformation;
