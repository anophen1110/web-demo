import React, {useState} from 'react';
import './../style.scss';
import useForm from 'react-hook-form';
import {Button, Container, Form} from "react-bootstrap";
import {Link} from 'react-router-dom';
import RadioButton from "../../../components/Button/RadioButton";
import CheckboxButton from "../../../components/Button/CheckboxButton";
import Label from "reactstrap/es/Label";

function ShippingMethod(props) {
    const methods = useForm();

    return(
        <div className='step'>
            <div className="helper-text">
                Please select a shipping method
            </div>
            <Form>
                <div className="item">
                    <Container>
                        <div className="inner-item">
                            <div className="one-row-two-item">
                                <div>
                                    <div className="font-weight-bold">Free</div>
                                    <div className="color-a7a7a7">$22</div>
                                </div>
                                <RadioButton name='billing'/>
                            </div>
                        </div>
                    </Container>
                </div>
                <div className="item">
                    <Container>
                        <div className="inner-item">
                            <div className="one-row-two-item">
                                <div>
                                    <div className="font-weight-bold">Fixed</div>
                                    <div className="color-a7a7a7">$0</div>
                                </div>
                                <RadioButton name='billing'/>
                            </div>
                        </div>
                    </Container>
                </div>
            </Form>
            <div className='wr-button'>
                <Container>
                    <div className="inner-wr-button one-row-two-item">
                        <Button
                            onClick={props.handleBack}
                            variant='secondary'
                            className=''
                        >
                            Previous
                        </Button>
                        <Button variant="primary" onClick={props.handleNext}>
                            Next
                        </Button>
                    </div>
                </Container>
            </div>
        </div>
    )
}
export default ShippingMethod;
