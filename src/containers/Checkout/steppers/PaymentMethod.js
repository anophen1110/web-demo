import React, {useState} from 'react';
import './../style.scss';
import useForm from 'react-hook-form';
import {Button, Container, Form} from "react-bootstrap";
import {Link} from 'react-router-dom';
import RadioButton from "../../../components/Button/RadioButton";
import CheckboxButton from "../../../components/Button/CheckboxButton";
import Label from "reactstrap/es/Label";

function PaymentMethod(props) {
    const methods = useForm();

    return(
        <div className='step'>
            <div className="helper-text">
                Please select payment method
            </div>
            <Form>
                <div className="item">
                    <Container>
                        <div className="inner-item">
                            <div className="one-row-two-item">
                                <div>
                                    <div className="font-weight-bold">Cash On Delivery</div>
                                    <div className="color-a7a7a7">Your payment will be collected by the shipping company when the order is delivered. Payment can be made by cash, certified check or money order.</div>
                                </div>
                                <RadioButton name='billing'/>
                            </div>
                        </div>
                    </Container>
                </div>
                <div className="item">
                    <Container>
                        <div className="inner-item">
                            <div className="one-row-two-item">
                                <div>
                                    <div className="font-weight-bold">FiPayPal Express Checkout xed</div>
                                </div>
                                <RadioButton name='billing'/>
                            </div>
                        </div>
                    </Container>
                </div>
                <div className="item">
                    <Container>
                        <div className="inner-item">
                            <div className="one-row-two-item">
                                <div>
                                    <div className="font-weight-bold">Purchase Order</div>
                                </div>
                                <RadioButton name='billing'/>
                            </div>
                        </div>
                    </Container>
                </div>
                <div className="item">
                    <Container>
                        <div className="inner-item">
                            <div className="one-row-two-item">
                                <div>
                                    <div className="font-weight-bold">Check / Money order</div>
                                </div>
                                <RadioButton name='billing'/>
                            </div>
                        </div>
                    </Container>
                </div>
                <div className="item">
                    <Container>
                        <div className="inner-item">
                            <div className="one-row-two-item">
                                <div>
                                    <div className="font-weight-bold">Credit Card (Braintree) - Test</div>
                                </div>
                                <RadioButton name='billing'/>
                            </div>
                        </div>
                    </Container>
                </div>
                <div className="item">
                    <Container>
                        <div className="inner-item">
                            <div className="one-row-two-item">
                                <div>
                                    <div className="font-weight-bold">Credit Card (saved)</div>
                                </div>
                                <RadioButton name='billing'/>
                            </div>
                        </div>
                    </Container>
                </div>
                <div className="item">
                    <Container>
                        <div className="inner-item">
                            <div className="one-row-two-item">
                                <div>
                                    <div className="font-weight-bold">Bank Transfer Payment</div>
                                </div>
                                <RadioButton name='billing'/>
                            </div>
                        </div>
                    </Container>
                </div>
                <div className="item">
                    <Container>
                        <div className="inner-item">
                            <div className="one-row-two-item">
                                <div>
                                    <div className="font-weight-bold">Pay by Braintree</div>
                                </div>
                                <RadioButton name='billing'/>
                            </div>
                        </div>
                    </Container>
                </div>
            </Form>
            <div className='wr-button'>
                <Container>
                    <div className="inner-wr-button one-row-two-item">
                        <Button
                            onClick={props.handleBack}
                            variant='secondary'
                            className=''
                        >
                            Previous
                        </Button>
                        <Button variant="primary" onClick={props.handleNext}>
                            Next
                        </Button>
                    </div>
                </Container>
            </div>
        </div>
    )
}
export default PaymentMethod;
