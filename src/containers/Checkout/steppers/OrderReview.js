import React, {createRef, useEffect, useState} from 'react';
import './../style.scss';
import useForm from 'react-hook-form';
import {Button, Container, Form, Image} from "react-bootstrap";
import {Link} from 'react-router-dom';
import RadioButton from "../../../components/Button/RadioButton";
import {ListProduct} from '../../ProductList/index';
import _ from 'lodash';
import CheckboxButton from "../../../components/Button/CheckboxButton";
import Label from "reactstrap/es/Label";
function OrderReview(props) {
    const refConfirm = createRef();
    const methods = useForm();
    const [sub, setSub] = useState(0);
    const [disableButton, setDisableButton] = useState(true);

    useEffect(()=>{
       let sub= _.reduce(_.map(ListProduct,(item,index)=>{
          return item.qty*item.newPrice;
       }),(accumulator,item)=>{
           return accumulator+item;
       });
       setSub(sub);
    },[ListProduct]);

    const onChangeCheckbox = () =>{
        if(refConfirm.current.checked){
            setDisableButton(false);
        }else{
            setDisableButton(true);
        }
    };
    return(
        <div className='step'>
            <div className="helper-text">
                Please select a billing address
            </div>
            <Form>
                <div className="item">
                    <Container>
                        <div className="inner-item">
                            <div className="one-row-two-item">
                                <div>
                                    <div className="font-weight-bold">Billing Information</div>
                                    <div className="color-a7a7a7">F542+96 La Veguada, Panama</div>
                                    <div className="color-a7a7a7">Calobre District</div>
                                    <div className="color-a7a7a7">Panama</div>
                                    <Link to={'#'}>0987654321</Link>
                                </div>
                                <div className="color-0076ff cursor-pointer">Edit</div>
                            </div>
                        </div>
                    </Container>
                </div>
                <div className="item">
                    <Container>
                        <div className="inner-item">
                            <div className="one-row-two-item">
                                <div>
                                    <div className="font-weight-bold">Shipping Information</div>
                                    <div className="color-a7a7a7">F542+96 La Veguada, Panama</div>
                                    <div className="color-a7a7a7">Calobre District</div>
                                    <div className="color-a7a7a7">Panama</div>
                                    <Link to={'#'}>0987654321</Link>
                                </div>
                                <div className="color-0076ff cursor-pointer">Edit</div>
                            </div>
                        </div>
                    </Container>
                </div>
                <div className="item">
                    <Container>
                        <div className="inner-item">
                            <div className="one-row-two-item">
                                <div>
                                    <div className="font-weight-bold">Shipping Method</div>
                                    <div className="color-a7a7a7">F542+96 La Veguada, Panama</div>
                                    <div className="color-a7a7a7">Calobre District</div>
                                    <div className="color-a7a7a7">Panama</div>
                                    <Link to={'#'}>0987654321</Link>
                                </div>
                                <div className="color-0076ff cursor-pointer">Edit</div>
                            </div>
                        </div>
                    </Container>
                </div>
                <div className="item">
                    <Container>
                        <div className="inner-item">
                            <div className="one-row-two-item">
                                <div>
                                    <div className="font-weight-bold">Payment Method</div>
                                    <div className="color-a7a7a7">F542+96 La Veguada, Panama</div>
                                    <div className="color-a7a7a7">Calobre District</div>
                                    <div className="color-a7a7a7">Panama</div>
                                    <Link to={'#'}>0987654321</Link>
                                </div>
                                <div className="color-0076ff cursor-pointer">Edit</div>
                            </div>
                        </div>
                    </Container>
                </div>
                <div className="item">
                    <Container>
                        <div className="inner-item">
                            <div className='order-summary'>
                                <div className="font-weight-bold">Order Summary</div>
                                {
                                    _.map(ListProduct, (item, index)=>{
                                       return(
                                           <div className='product' key={index}>
                                               <div className="img"><Image src={item.image}/></div>
                                               <div>
                                                   <div className=' mb-1'>{item.name}</div>
                                                   <div className=''>Quantity: {item.qty}</div>
                                                   <div className=''>Price: {item.newPrice}</div>
                                                   <div className=''>Subtotal: {item.newPrice*item.qty}</div>
                                               </div>
                                           </div>
                                       )
                                    })
                                }
                                <div className='mt-15px mb-15px'>
                                    <div className="one-row-two-item">
                                        <div className="fs-16 mb-10px">Subtotal ({ListProduct.length}):</div>
                                        <div>{sub}</div>
                                    </div>
                                    <div className="one-row-two-item">
                                        <div className="fs-16">Shipping:</div>
                                        <div>#3333</div>
                                    </div>
                                    <div className="one-row-two-item font-weight-bold">
                                        <div className="fs-16">Fixed:</div>
                                        <div>#3333</div>
                                    </div>
                                    <div className="one-row-two-item font-weight-bold mt-10px">
                                        <div className="fs-16">Total Amount Payable:</div>
                                        <div>#3333</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </Container>
                </div>
            </Form>
            <div className='wr-button'>
                <Container>
                    <div className={'wr-confirm'}>
                        <CheckboxButton onChangeCheckbox={onChangeCheckbox} id='confirm' className='square-box-blue' name='confirm' innerRef={refConfirm}/>
                        <Label for={'confirm'}>I acknowledge that I have an obligation to pay for this item and agree with the general terms & conditions</Label>
                    </div>
                    <div className="inner-wr-button one-row-two-item">
                       <Button disabled={disableButton}>Place Order</Button>
                    </div>
                </Container>
            </div>
        </div>
    )
}
export default OrderReview;
