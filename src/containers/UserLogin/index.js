import React, {useState, useEffect} from 'react';
import {Container, Row, Col, Image,Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';
import {FormGroupS} from '../../components/FormGroup/index';
import {StyledForm, SpanEye, SpanClose} from '../DevLogin/StyledDevLogin';
import InputS from '../../components/FormGroup/Input';
import LabelS from '../../components/FormGroup/Label';
import SelectS from '../../components/Select';
import './style.scss';
import '../../Style/global.scss';
import ic_close from "../../Assets/assets/ic_close.png";
import ic_close2x from "../../Assets/assets/ic_close@2x.png";
import ic_password from "../../Assets/assets/ic_password.png";
import ic_password_hide from "../../Assets/assets/ic_password_hide.png";
import * as history from "history";

function UserLogin() {



    const [isShowP, setIsShowP] = useState(true);
    const [isCloseP, setIsCloseP] = useState(false);
    const [passValue, setPassValue] = useState("");

    const isShowPassword = () =>{
        setIsShowP(!isShowP);
    };

    return(
        <div className="wr-user-login">
            <div className="wr-title">
                <Container>
                    <div className="wr-title-inner">
                        <div className="title">Sign in</div>
                        <div className="sub-title">Don’t have an account? <Link to={'/register'}>Create now</Link></div>
                        <span className="btn-close cursor-pointer" onClick={()=>history.createBrowserHistory().goBack()}><Image src={ic_close2x}/></span>
                    </div>
                </Container>
            </div>
            <div className="wr-content">
                <Container>
                    <StyledForm >
                        <div className="m-auto">
                            <FormGroupS>
                                <LabelS text="email"/>
                                <InputS type="text" name="username" placeholder="User Name" />
                            </FormGroupS>
                            <FormGroupS className="position-relative">
                                <LabelS text="password"/>
                                <InputS type={isShowP?"password":"text"} onChange={e=>{if(e.target.value !=null){setIsCloseP(true)}}} name="password" placeholder="password" />
                                {isCloseP?<SpanClose><Image src={ic_close} /></SpanClose>:""}
                                <SpanEye onClick={isShowPassword}><Image src={isShowP?ic_password:ic_password_hide} /></SpanEye>
                            </FormGroupS>
                            <div className="pr-3 pl-3">
                                <Button className="w-100 bg-373737" variant="secondary" type="submit">Sign in</Button>
                            </div>
                            <div className="text-forgot text-center">
                                <Link>Forgot your password?</Link>
                            </div>
                        </div>
                    </StyledForm>
                </Container>
            </div>
        </div>
    )
}

export default UserLogin;
