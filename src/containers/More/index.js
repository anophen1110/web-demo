import React, {useState} from 'react';
import {NavLink, Link} from 'react-router-dom';
import {Button, Row, Col, Image, Container, ListGroup, ListGroupItem} from 'react-bootstrap';
import _ from 'lodash';
import './style.scss';
import ic_avatar from '../../Assets/assets/ic_avatar.png';
import ic_push_message from '../../Assets/assets/ic_push_message.png';
import av_home from '../../Assets/assets/item_home_screen_1.png';
import av_list from '../../Assets/assets/jmango_mug_1.png';
import ButtonBack from "../../components/Button/ButtonBack";
function More() {

    const list=[
        {
            module:'/module1',
            thumbnail:av_list,
            name:'Name1'
        },
        {
            module:'/module2',
            thumbnail:av_list,
            name:'Name2'
        },
        {
            module:'/module3',
            thumbnail:av_list,
            name:'Name3'
        },
        {
            module:'/module4',
            thumbnail:av_list,
            name:'Name4'
        },
        {
            module:'/module5',
            thumbnail:av_list,
            name:'Name5'
        },
        {
            module:'/module6',
            thumbnail:av_list,
            name:'Name6'
        },
        {
            module:'/module7',
            thumbnail:av_list,
            name:'Name7'
        },
        {
            module:'/module8',
            thumbnail:av_list,
            name:'Name8'
        },
        {
            module:'/module9',
            thumbnail:av_list,
            name:'Name9'
        },
        {
            module:'/module10',
            thumbnail:av_list,
            name:'Name10'
        },
    ];

    return (
        <div className='wr-more'>
            <div className="bg-rgb249 bar-login position-relative one-row-two-item">
                <ButtonBack/>
                <div className='d-flex-column text-center'>
                    <Link to={'/my-profile'}>
                        <div className="avatar">
                            <Image src={ic_avatar}/>
                        </div>
                    </Link>
                    <Link className="link" to={`/user-login`}>Log In <i className="fa fa-angle-right" aria-hidden="true"></i></Link>
                </div>
                <Link className="push-message" to={`/push-message-list`}><Image src={ic_push_message}/></Link>
            </div>
            <ListGroup className="list-group">
                <NavLink className="nav-link" to={`/home`}>
                    <ListGroupItem className="list-group-item d-flex-row bg-rgb249">
                        <div className="item-image">
                            <Image src={av_home}/>
                        </div>
                        <div className="item-name">
                            Home Screen
                        </div>
                        <i className="fa fa-angle-right" aria-hidden="true"></i>
                    </ListGroupItem>
                </NavLink>
                {
                    _.map(list,(item,index)=>{
                        return <NavLink className="nav-link" to={`/product-list`}>
                            <ListGroupItem className="list-group-item d-flex-row">
                                <div className="item-image">
                                    <Image src={item.thumbnail}/>
                                </div>
                                <div className="item-name">
                                    {item.name}
                                </div>
                                <i className="fa fa-angle-right" aria-hidden="true"></i>
                            </ListGroupItem>
                        </NavLink>
                    })
                }
            </ListGroup>
        </div>
    )
}

export default More;
