import React, {useEffect, useState} from 'react';
import {Link, Redirect} from 'react-router-dom';
import {Button, Row, Col, Image, Container, Modal, Form} from 'react-bootstrap';
import _ from 'lodash';
import './style.scss';
import imgApp from '../../Assets/assets/AppIcon.png';
import imgKey from '../../Assets/assets/ic_key/ic_key.png';
import Disconnect from '../Disconnect';
import useForm from "react-hook-form";
import InputS from "../../components/FormGroup/Input";
import PropTypes from "prop-types";
import {callApiACT} from "../../redux/callApiRDC";
import {connect} from "react-redux";
import {compose} from "redux";
import * as AppURL from "../../Services/urlAPI";
import {reducerNameHOR} from '../../redux';
import * as Constants from '../../Constants';
import { HeaderKey, TypeRequest } from "../../Constants";
import * as Utils from '../../utils';
import reactLocalStorage from "../../utils/LocalStorage";
import GlobalSettingApp from '../../Config/GlobalSettingApp';
import * as Services from "../../Services";
import {setGlobalAPI} from "../../Services/callDataAPI";

function ListApp(props) {

    const methods = useForm();
    const [modal, setModal] = useState(false);
    const [connection, setConnection] = useState(true);
    const [value, setValue] = useState(0);
    const [loadingList, setLoading] = useState([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]);
    const [listApp, setListApp] = useState([{ items: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10] }]);
    const [isFetching, setIsFetching] = useState(true);
    const [isRefresh, setIsRefresh] = useState(false);
    const [pageNum, setPageNum] = useState(0);
    const [isOnline, setIsOnline] = useState(true);
    const [errMessage, setErrMessage] = useState("");
    const [inputKeyText, setInputKeyText] = useState("");
    const [isInvalidKey, setIsInvalidKey] = useState(false);
    const [userNameDev, setUserNameDev] = useState("");
    const [oneApp, setOneApp] = useState({});
    const [redirect, setRedirect] = useState(false);

    const setHeader =async()=>{
        const headerObj = await reactLocalStorage.getObject(
            Constants.KeyAsyncStore.headerDevLogin
        );
        const headerParse = Utils.safeParseJson(headerObj);
        setGlobalAPI(undefined, headerParse);

        const userNameDev = await reactLocalStorage.getObject(
            Constants.KeyAsyncStore.userNameDev
        );
        setUserNameDev(getListApp());

    };
     useEffect(()=>{
         setHeader();
     },[]);

    const getListApp = async (isRefresh = false) => {
        props.callApiACT({
                url: AppURL.getListApp,
                name: reducerNameHOR.getListApp,
                typeRequest: TypeRequest.GET,
                isRefresh
            })
            .then(res => {
                let listAppData = Utils.getSafeValue(res, "apps", []);

                setListApp(listAppData);
                setIsRefresh(false);
                setIsFetching(false);
                setIsOnline(true);
            })
            .catch(errMessage => {
                setIsRefresh(false);
                setIsFetching(false);
                setIsOnline(true);
            });
    };
    const checkKeyValid = async item => {
        let keyDevice = null;
        const appTypeCode = item.appTypeCode;
        const appKey = item.appKey;
        let params = {
            appKey,
            deviceKey: keyDevice,
            appTypeCode,
            devicefingerprint: Services.fingerPrint()
        };
        props.callApiACT({
                url: Utils.replaceStrUrl(AppURL.registerApp, [appTypeCode]),
                typeRequest: TypeRequest.POST,
                name: reducerNameHOR.registerApp,
                params
            })
            .then(async res => {
                const appData = Utils.getSafeValue(res, "appData", {});
                const deviceKey = Utils.getSafeValue(res, "deviceKey", "");

                //save appsetting
                let appMetadata = Utils.getSafeValue(appData, "appMetadata", {});
                let settingApp = Utils.getSafeValue(appMetadata, "data", "");
                GlobalSettingApp.settingApp = Utils.safeParseJson(settingApp);
                reactLocalStorage.setObject(
                    Constants.KeyAsyncStore.globalSetting,
                    settingApp
                );

                var listModule = Utils.getSafeValue(appData, "modules", []);
                //save listModule to AsyncStore
                await reactLocalStorage.setObject(
                    Constants.KeyAsyncStore.listModule,
                    JSON.stringify(listModule)
                );

                //save UserAuth
                let userAuthModule = listModule.find(obj => {
                    return obj.moduleType === Constants.ModuleType.USER_AUTH;
                });

                let userAuthMetaData = Utils.getSafeValue(
                    userAuthModule,
                    "moduleMetaData",
                    {}
                );
                let userAuth = Utils.getSafeValue(userAuthMetaData, "data", "");
                GlobalSettingApp.userAuth = Utils.safeParseJson(userAuth);

                await reactLocalStorage.setObject(
                    Constants.KeyAsyncStore.deviceKey,
                    deviceKey
                );

                this.setState({ isInvalidKey: false }, () => {
                    // this.props.navigation.navigate(RoutesName.AppStack);
                    this.getListLanguage(appKey, appTypeCode);
                });
            })
            .catch(errMessage => {
                setIsInvalidKey(()=>{})
            });
        setModal(false);
        setRedirect(true);
    };

    useEffect(()=>{
        if(listApp.length===0){setConnection(false);}
    },[listApp]);

    const onClickModalAdded = (item) =>{
        setModal(true);
        setOneApp(item);
    };

    useEffect(()=>{
        methods.reset(_.pick(oneApp,['appKey', 'appType', 'appTypeCode']));
    },[oneApp]);


    return(
        <div className="wr-list-app">
            {redirect===true&&<Redirect to={'/home'}/>}
            <div className="top-bar d-flex flex-row">
                <div className="text">qavn@jmango360.com</div>
                <a href="">Log Out</a>
            </div>
            <Container>
            {
                !connection? <Disconnect/>
                    :
                    <Row>
                    {
                        _.map(listApp,(item, index)=>{
                            return <Col md={6} key={index}>
                                <div className="one-app mt-3 fs-14" >
                                    <div className="img mr-3"><Image src={item.appIcon} /></div>
                                    <div>
                                        <b>{item.appName}</b>
                                        <div>AppKey: {item.appKey}</div>
                                        <div>AppType: {item.appType}</div>
                                        <div>AppTypeCode: {item.appTypeCode}</div>
                                        <Button onClick={()=>onClickModalAdded(item)}>Click</Button>
                                    </div>
                                </div>
                            </Col>;
                        })
                    }
                    <Button onClick={()=>setModal(true)} className="btn-input-key">
                        <Image  src={imgKey} alt="ss"/>
                    </Button>
                    <Modal dialogClassName="style-modal" show={modal} onHide={()=>setModal(false)}>
                        <Form onSubmit={methods.handleSubmit(checkKeyValid)}>
                            <Modal.Header >Enter application info</Modal.Header>
                            <Modal.Body>
                                <InputS className='mb-10px' type="text" name="appKey" placeholder="Enter App Key here..." innerRef={methods.register}/>
                                <InputS className='mb-10px' type="text" name="appType" placeholder="int/uat/prod" innerRef={methods.register}/>
                                <InputS className='mb-10px' type="text" name="appTypeCode" placeholder="App Type Code" innerRef={methods.register}/>
                            </Modal.Body>
                            <Modal.Footer>
                                <Button variant="primary"  type="submit">OK</Button>
                            </Modal.Footer>
                        </Form>
                    </Modal>
                </Row>
            }
            </Container>
        </div>
    )
}
ListApp.propTypes = {
    callApiACT: PropTypes.func,
};

function mapStateToProps(state) {
    return{}
}

function mapDispatchToProps(dispatch) {
    return {
        callApiACT: params => dispatch(callApiACT(params)),
    };
}
const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps,
);
export default compose(
    withConnect
)(ListApp);

