import React, {useState} from 'react';
import './style.scss';
import { Container} from "react-bootstrap";
import {ListProduct} from '../ProductList/index';
import _ from 'lodash';
import ItemWishlist from "./ItemWishlist";
import ButtonBack from "../../components/Button/ButtonBack";

function MyWishlist() {

    return(
        <div className="wr-my-wishlist">
            <div className="top-bar-title">
                <Container>
                    <div className="one-row-two-item">
                        <ButtonBack/>
                        <div className="text-center position-relative">
                            My Wishlist
                        </div>
                        <>&nbsp;</>
                    </div>
                </Container>
            </div>
            {
                ListProduct.length>0&&<div className="items">
                    {
                        _.map(ListProduct,(item,index)=>{
                            return <ItemWishlist index={index} key={index} item={item}/>
                        })
                    }
                </div>
            }
            {
                ListProduct.length===0&&<div className="no-item">
                    There are no items in your wishlist
                </div>
            }
        </div>
    )
}
export default MyWishlist;
