import React, {createRef, useState} from 'react';
import useForm from 'react-hook-form';
import {Container, Image, Button, Form} from 'react-bootstrap';
import InputS from "../../components/FormGroup/Input";
import InputWrapLabel from "../../components/Input/InputWrapLabel";
import ButtonTrash from "../../components/Button/ButtonTrash";
import ButtonAddToCart from "../../components/Button/ButtonAddToCart";

function ItemWishlist(props) {
    const methods= useForm();
    const [note, setNote] = useState('');
    const [showTypeNote, setShowTypeNote] = useState(false);
    const refNote = createRef();
    const addNote = () =>{
        setNote(methods.getValues().note);
        setShowTypeNote(false);
    };
    const onClickButtonNote = ()=>{
        setShowTypeNote(true);
        setTimeout(()=>{
            methods.setValue('note',note)
        });
    };

    return(
        <div className="item">
            <Container>
                <div className="inner-item">
                    <div className="one-row-two-item">
                        <div className="one-row-two-item">
                            <div className="img">
                                <Image src={props.item.image}/>
                            </div>
                            <div className="detail">
                                <div className="font-weight-bold">{props.item.name}</div>
                                <div className="color-a7a7a7">Price: {props.item.newPrice}</div>
                                {note!='' && <><span className='font-weight-bold'>Note: </span><span className='fs-14'>{note}</span></>}
                            </div>
                        </div>
                        <div className="item-right">
                            <ButtonAddToCart/>
                            <ButtonTrash/>
                            <div onClick={onClickButtonNote} className="note cursor-pointer">Note</div>
                        </div>
                    </div>
                    {showTypeNote===true && <>
                        <InputWrapLabel innerRef={methods.register} type='textarea' name={`note`} title='Note'/>
                        <Button onClick={addNote}>Add</Button>
                    </>}
                </div>
            </Container>
        </div>
    )
}
export default ItemWishlist;
