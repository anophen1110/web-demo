import React, {useState} from 'react';
import './style.scss';
import * as history from 'history';
import { Container} from "react-bootstrap";
import ProductList, {ListProduct} from '../ProductList/index';
import _ from 'lodash';
import Input from "reactstrap/es/Input";
import {Button,Form} from "react-bootstrap";
import ButtonClose from "../../components/Button/ButtonClose";
import {Route} from "react-router";
import ButtonBack from "../../components/Button/ButtonBack";

function SearchProduct() {
    const [dpRecent, setDpRecent] = useState(false);

    return(
        <div className="wr-search">
            <div className="top-bar-title">
                <Container>
                    <div className="text-center position-relative">
                        <Form>
                            <div className="search-box">
                                <ButtonBack/>
                                <div className="inner-box">
                                    <i className='fa fa-search'></i>
                                    <Input placeholder='Search...' name='search' onFocus={()=>setDpRecent(true)} type='text' innerRef={e =>{const refSearch=e}  } />
                                </div>
                                <Button variant='secondary' onClick={()=>history.createBrowserHistory().goBack()} type='button'>Cancel</Button>
                            </div>
                        </Form>
                    </div>
                </Container>
            </div>
            <div className={'items '+(dpRecent?'':'most-recent ')}>
                <Container>
                    <div className="one-row-two-item">
                        <h4 className='p-10px'>MOST RECENT</h4>
                        <ButtonClose onClick={()=>setDpRecent(false )}/>
                    </div>
                    <div className="item">
                        <div className="inner-item p-15px">
                            sdfg
                        </div>
                        <div className="inner-item p-15px">
                            sdfg
                        </div>
                        <div className="inner-item p-15px">
                            sdfg
                        </div>
                    </div>
                </Container>
            </div>
            {
                ListProduct.length===0&&<div className="no-item">
                    No item found
                </div>
            }
        </div>
    )
}
export default SearchProduct;
