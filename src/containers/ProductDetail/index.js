import React, {useState} from 'react';
import _ from 'lodash';
import CarouselS from "../../components/Carousel";
import {Button, Card, Col, Container, Image, Row} from "react-bootstrap";
import InputQty from '../../components/Input/InputQty';

import './style.scss';
import '../../Style/global.scss';
import '../ShoppingCart/style.scss';
import Accordion from "react-bootstrap/Accordion";
import ic_cart_detail from '../../Assets/assets/ic_cart_detail.png';
import Rating from "../../components/Rating";
import RelatedProduct from "./related";
import ic_product_default3x from "../../Assets/assets/ic_product_default3x.png";
import ModalReview from "./ModalReview";
import SelectWrap from "../../components/SelectWrapLabel";
import RadioButton from "../../components/Button/RadioButton";
import CheckboxButton from "../../components/Button/CheckboxButton";
import InputWrapLabel from "../../components/Input/InputWrapLabel";
import InputStyle from "../../components/Input";

function ProductDetail() {
    const ListProduct =[
        {
            name:'name 1',
            image:ic_product_default3x,
            oldPrice:'20',
            newPrice:'15'
        },
        {
            name:'name 2',
            image:ic_product_default3x,
            oldPrice:'20',
            newPrice:'15'
        },
        {
            name:'name 3',
            image:ic_product_default3x,
            oldPrice:'20',
            newPrice:'15'
        },
        {
            name:'name 4',
            image:ic_product_default3x,
            oldPrice:'20',
            newPrice:'15'
        },
        {
            name:'name 5',
            image:ic_product_default3x,
            oldPrice:'20',
            newPrice:'15'
        },
        {
            name:'name 6',
            image:ic_product_default3x,
            oldPrice:'20',
            newPrice:'15'
        },
        {
            name:'name 7',
            image:ic_product_default3x,
            oldPrice:'20',
            newPrice:'15'
        },
        {
            name:'name 8',
            image:ic_product_default3x,
            oldPrice:'20',
            newPrice:'15'
        },
    ];
    const ListSelect =[
        {
            label:'1',
            value:'1'
        },
        {
            label:'2',
            value:'2'
        },
        {
            label:'3',
            value:'3'
        }
    ];
    const ListSize = ['S', 'M', 'L', 'XL', 'XXL'];
    const ListColor = ['red', 'black', 'blue'];

    const [showModalReview, setShowModalReview] = useState(false);
    const [valueColor, setValueColor] = useState('');
    const [valueSize, setValueSize] = useState('');
    const [itemSize, setItemSize] = useState(null);
    const [itemColor, setItemColor] = useState(null);

    const onChangeColor = (value) =>{
        setValueColor(value);
    };
    const onChangeSize = (value) =>{
        setValueSize(value);
    };

    return(
        <div className='wr-product-detail'>
            <div className="top-bar-title">
                Detail
            </div>
            <Container>
                <CarouselS/>
            </Container>
            <div className="inner-wr text-center">
                <Container>
                    <div className="pt-10px pb-10px fs-14">
                        <div className="color-rgb117">Brand Name</div>
                        <div className="fs-18">This is product name example and it will never get truncated at all</div>
                        <div className="m-15px"><span className='fw-500'>SKU:</span>ET555</div>
                        <div className="m-15px">
                            <span className='fw-500 position-relative'>
                                 Color:&nbsp;{itemColor!=null?itemColor:<div className='tooltip-note'>Please select your color</div>}
                            </span>
                        </div>
                        <div className="row-gender">
                            {
                                _.map(ListColor, (item, index)=>{
                                    return  <label key={index} onClick={()=>setItemColor(item)} className="value ml-2">
                                        <input type="radio" name='color-radio'/>
                                        <span style={{backgroundColor:item}} className="span-color checkmark"></span>
                                    </label>
                                })
                            }
                        </div>
                        <div className="m-15px">
                            <span className='fw-500 position-relative'>
                                Size:&nbsp;{itemSize!=null?itemSize:<div className='tooltip-note'>Please select your size</div>}
                            </span>
                        </div>
                        <div className="row-gender">
                            {
                                _.map(ListSize, (item, index)=>{
                                    return  <label key={index} onClick={()=>setItemSize(item)} className="value ml-2">
                                        <input type="radio" name='size-radio'/>
                                        <span style={{backgroundColor:item}} className="span-size checkmark">{item}</span>
                                    </label>
                                })
                            }
                        </div>

                        <SelectWrap
                            label="color"
                            options={ListSelect}
                            valueColor={valueColor}
                            setValueColor={setValueColor}
                            onChange={onChangeColor}
                            name='color'
                            placeholder="Choose Select"
                        />
                        <SelectWrap
                            label="size"
                            options={ListSelect}
                            value={valueSize}
                            onChange={ onChangeSize}
                            name='size'
                            placeholder="Choose Select"
                        />
                        <span className="fs-18 color-rgb117 text-decoration-through">AU$456</span> <span className="fs-18 fw-500">AU$456</span>
                        <table className='price-from-to'>
                            <tbody>
                                <tr><td>FROM</td><td>AU$1099 </td></tr>
                                <tr><td>TO</td><td>AU$1200 </td></tr>
                            </tbody>
                        </table>
                        <div className="m-15px"><InputQty type='number'/></div>
                        <div className="item-out-of-stock">Sorry, this item is out of stock</div>
                        <p className="fs-12 color-10710454">Buy 3 or more for <span className="fw-500">AU$50.00</span> each and <span className="fw-500">save 50%</span></p>
                        <Button className="btn-add-to-bag"><Image className="img-in-btn" src={ic_cart_detail}/><span>Add to Bag</span></Button>
                        <div className="bundle-product-empty">
                            <div className="text-required">*Required Field</div>
                            <div className="group">
                                <div className="title-block mb-10px">Radio Button*</div>
                                <div className="gr-radio-label mb-10px">
                                    <RadioButton id='id1' name='namestar'/>
                                    <label htmlFor="id1">Short text&nbsp;<span className='color-rgb137'>+$1.00</span></label>
                                </div>
                                <div className="gr-radio-label mb-10px">
                                    <RadioButton id='id2' name='namestar'/>
                                    <label htmlFor="id2">Long text example and it go for another line, so it has no truncation at all&nbsp;<span className='color-rgb137'>-$1.00</span></label>
                                </div>
                                <div className="gr-radio-label mb-10px">
                                    <RadioButton id='id3' name='namestar'/>
                                    <label htmlFor="id3">Short text&nbsp;<span className='color-rgb137'>-$1.00</span></label>
                                </div>
                            </div>
                            <div className="group">
                                <div className="title-block mb-10px">Checkbox and Multiple Select*</div>
                                <div className="gr-radio-label mb-10px">
                                    <CheckboxButton className='square-box' id='cb1' name='namestar1'/>
                                    <label htmlFor="cb1">Short text&nbsp;<span className='color-rgb137'>+$1.00</span></label>
                                </div>
                                <div className="gr-radio-label mb-10px">
                                    <CheckboxButton id='cb2' className='square-box' name='namestar2'/>
                                    <label htmlFor="cb2">Long text example and it go for another line, so it has no truncation at all&nbsp;<span className='color-rgb137'>-$1.00</span></label>
                                </div>
                                <div className="gr-radio-label mb-10px">
                                    <CheckboxButton id='cb3' className='square-box' name='namestar3'/>
                                    <label htmlFor="cb3">Short text&nbsp;<span className='color-rgb137'>-$1.00</span></label>
                                </div>
                            </div>
                            <div className="group">
                                <SelectWrap
                                    label='Dropdown*'
                                />
                                <div className="gr-radio-label">
                                    Qty: <InputStyle className='mb-0 ml-2' value='1' type='number'/>
                                </div>
                            </div>
                            <div className="group">
                                <SelectWrap
                                    label='Dropdown*'
                                />
                                <div className="gr-radio-label">
                                    Qty: <span className='fw-500 ml-2'>1</span>
                                </div>
                            </div>
                            <div className="group">
                                <div className="title-block">Video Capture Device*</div>
                                <div className="gr-radio-label">
                                    <div>Canon 5D Mark IV camera <span className='color-rgb137'>$15.00</span></div>
                                </div>
                                <InputWrapLabel type='text' title='Text box*' labelValue='$15.00' placeholder='Please enter something here...'/>
                            </div>
                            <div className="group">
                                <InputWrapLabel type='textarea' maxlength='500' title='Text box*' labelValue='$15.00'  placeholder='Please enter something here...' />
                            </div>

                        </div>
                    </div>
                </Container>
            </div>
            <div className="inner-wr">
                <Container>
                    <div className="fs-14 pt-10px pb-10px">
                        Lorem Ipsum of florem geeft een diepe reiniging van je kostbare make up kwasten. De cleansing gel verwijdert alle make up resten die je ziet en ook die je niet kunt zien. This is just example of short description
                        <br/>
                        Lorem Ipsum of florem geeft een diepe reiniging van je kostbare make up kwasten. De cleansing gel verwijdert alle make up resten die je ziet en ook die je niet kunt zien. This is just example of short description
                        <div className="show-less">Show Less <i className="fa fa-angle-double-up" aria-hidden="true"></i></div>
                    </div>
                </Container>

            </div>
            <div className="inner-wr">
                <Container>
                    <div className="fs-14 pt-10px pb-10px">
                        <Accordion  defaultActiveKey="0">
                            <Card>
                                <Accordion.Toggle className='bg-fff' as={Card.Header} eventKey="1">
                                    <div className="fw-500">Additional Information</div>
                                    <i className="fa fa-angle-down" aria-hidden="true"></i>
                                </Accordion.Toggle>
                                <Accordion.Collapse eventKey="1">
                                    <Card.Body>
                                        Detail
                                    </Card.Body>
                                </Accordion.Collapse>
                            </Card>
                        </Accordion>
                    </div>
                </Container>
            </div>
            <div className="inner-wr">
                <Container>
                    <div className="fs-14 pt-10px pb-10px">
                        <Accordion  defaultActiveKey="0">
                            <Card>
                                <Accordion.Toggle className='bg-fff' as={Card.Header} eventKey="1">
                                    <div className="fw-500">Review</div>
                                    {/*<div className="color-rgb117 ml-auto mr-4">No Reviews</div>*/}
                                    <div className="color-rgb117 ml-auto mr-4">
                                        <Rating disabled={true}/>
                                    </div>
                                    <i className="fa fa-angle-down" aria-hidden="true"></i>
                                </Accordion.Toggle>
                                <Accordion.Collapse eventKey="1">
                                    <Card.Body>
                                        Detail
                                    </Card.Body>
                                </Accordion.Collapse>
                            </Card>
                        </Accordion>
                    </div>
                </Container>
            </div>
            <div className="inner-wr">
                <Container>
                    <div className="product-review-box">
                        <div className="no-review-desc">
                            Be the first to review this product
                        </div>
                        <div className="btn-add-review" onClick={()=>setShowModalReview(true)} >Add Your Review</div>
                        <ModalReview setShowModalReview={setShowModalReview} showModalReview={showModalReview}/>
                    </div>
                </Container>
            </div>
            <div className="inner-wr">
                <Container>
                    <RelatedProduct list={ListProduct}/>
                </Container>
            </div>
            <div className="bar-checkout d-none">
                <Container>
                    <Row>
                        <Col col={6}>
                            <div className="text-center pt-10px"><InputQty type='number'/></div>
                        </Col>
                        <Col col={6}>
                            <Button type="submit" className="fw-500 w-100">Checkout</Button>
                        </Col>
                    </Row>
                </Container>
            </div>
        </div>
    )
}
export default ProductDetail;
