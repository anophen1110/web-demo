import React, {useState} from 'react';
import GridPrd from "../../components/ListProduct/GridPrd";
import ButtonBackToTop from "../../components/Button/ButtonBackToTop";
import '../../components/ListProduct/style.scss';
import {Button, Col, Image, Row} from "react-bootstrap";
import _ from "lodash";
import {Link} from "react-router-dom";
import ButtonAddToCart from "../../components/Button/ButtonAddToCart";
import Rating from "../../components/Rating";
import ItemAddWishList from "../../components/ItemAddWishList";
import ButtonShowPrice from "../../components/Button/ButtonShowPrice";

function ItemProduct(props) {

    const [isShowPrice, setIsShowPrice]  = useState(true);

    const showPrice = () =>{
        setIsShowPrice(!isShowPrice);
    };

    const handleAddToCart = (item) =>{
        console.log(item);
    };
    return <Col className="p-0" xs={6}>
        <div className="wr-item">
            <div className="item">
                <div className="item-image">
                    <Link to={`product-detail`}><Image src={props.item.image}/></Link>
                    <ItemAddWishList/>
                </div>
                <div className="item-name"><Link to={`product-detail`}>{props.item.name}</Link></div>
                <Rating style={{ fontSize: 18 }} />
                <div className="price-box">
                    {isShowPrice && <ButtonShowPrice showPrice={showPrice}/>}
                    {!isShowPrice && <div className="item-price item-old-price">{props.item.oldPrice}</div>}
                    {!isShowPrice && <div className="item-price item-new-price">{props.item.newPrice}</div>}
                </div>
                {props.item.qty !=0 && <ButtonAddToCart handleAddToCart={handleAddToCart} item={props.item}/>}
                {/*{props.item.qty===0 && <div className='out-of-stock'>Out of Stock</div>}*/}
                {props.item.qty===0 && <div className='out-of-stock qty-0'>
                    <div className="label">Qty</div>
                    <div className="value">0</div>
                </div>}
            </div>
        </div>
    </Col>
}
function RelatedProduct(props) {


    return(
        <div className="grid-product">{ props && props.list &&
            <Row>
                {
                    _.map(props.list, (item, index)=>{
                        return <ItemProduct item={item} key={index} />
                    })
                }
            </Row>
            }
        </div>
    )
}
export default RelatedProduct;
