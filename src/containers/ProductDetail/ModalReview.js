import React, {useState} from 'react';
import {Modal, Button} from 'react-bootstrap';

import './style.scss';
import Rating from "../../components/Rating";
import InputS from "../../components/FormGroup/Input";

function ModalReview(props) {

    const handleClose = () =>{
      props.setShowModalReview(false);
    };

    const handleSubmit = () =>{
        props.setShowModalReview(false);
    };

    return(
        <Modal dialogClassName={'wr-product-detail'} size="lg" show={props.showModalReview} onHide={handleClose}>
            <Modal.Header closeButton>
                <Modal.Title>Review</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <div className="title-block">
                    <div className="desc">*Please fill in all required fields</div>
                    <div>How do you rate this products?</div>
                </div>
                <div className="review-field">
                    <div className="text-uppercase title-field">Rating*</div>
                    <Rating/>
                </div>
                <div className="review-field">
                    <div className="title-field">Nickname*</div>
                    <InputS type="text" name="nickname"/>
                </div>
                <div className="review-field">
                    <div className="title-field">Summary*</div>
                    <InputS className="txt_area" type="textarea" name="nickname"/>
                </div>
                <div className="review-field">
                    <div className="title-field">Review*</div>
                    <InputS className="txt_area" type="textarea" name="nickname"/>
                </div>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="primary" onClick={handleClose}>
                    Submit
                </Button>
            </Modal.Footer>
        </Modal>
    )
}
export default ModalReview;
