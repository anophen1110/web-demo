import React, {useState} from 'react';
import {Link} from 'react-router-dom';
import '../../Style/accordion.scss';
import './style.scss';
import {Button, Container, Modal, Card, Accordion} from "react-bootstrap";
import ButtonClose from '../../components/Button/ButtonClose';
import CheckboxButton from '../../components/Button/CheckboxButton/index';
import RadioButton from "../../components/Button/RadioButton";

function MyOrderDetail() {

    const [openSideFilter, setOpenSideFilter] = useState(false);

    const openSidebarFilter = () =>{
        setOpenSideFilter(!openSideFilter);
    };

    return(
        <div className="wr-my-order">
            <div className="top-bar-title">
                <Container>
                    <div className="text-center position-relative">
                        Order Detail
                    </div>
                </Container>
            </div>
            <div className="items">
                <div className="item">
                    <Container>
                        <div className="inner-item">
                           <table>
                               <tbody>
                                   <tr><td>Status:</td><td className='fw-500'>Pending</td></tr>
                                   <tr><td>Order $:</td><td>1234567</td></tr>
                                   <tr><td>Order Date:</td><td>16 Oct 2019</td></tr>
                               </tbody>
                           </table>
                        </div>
                    </Container>
                </div>
                <div className="item">
                    <Container>
                        <div className="inner-item">
                            <Accordion className='default'  defaultActiveKey="1">
                                <Card>
                                    <Accordion.Toggle className='bg-fff pr-0 pl-0' as={Card.Header} eventKey="1">
                                        <div className="fw-500">Order Summary(1 items)</div>
                                        <i className="fa fa-angle-down" aria-hidden="true"></i>
                                    </Accordion.Toggle>
                                    <Accordion.Collapse eventKey="1">
                                        <Card.Body>
                                            <div className="order-product">
                                                <div className="qty">1 x</div>
                                                <div className="product">
                                                    <div className="name-product">Bath Minerals and Salt</div>
                                                    <div className={'fs-14'}><span className='color-rgb137'>Price:</span>$25</div>
                                                    <div className={'fs-14'}><span className='color-rgb137'>Sku:</span>ABC-abc-8888</div>
                                                </div>
                                                <div className="price">$25</div>
                                            </div>
                                            <div className="price-summary">
                                                <div className="one-row">
                                                    <div className="label">Grand Total (Excl. Tax)</div>
                                                    <div className="value">$10</div>
                                                </div>
                                                <div className="one-row">
                                                    <div className="label">Subtotal</div>
                                                    <div className="value">$10</div>
                                                </div>
                                                <div className="one-row">
                                                    <div className="label">Shipping & Handling</div>
                                                    <div className="value">$10</div>
                                                </div>
                                                <div className="one-row">
                                                    <div className="label">Tax (%)</div>
                                                    <div className="value">$10</div>
                                                </div>
                                                <div className="one-row total">
                                                    <div className="label">Grand Total (Incl. Tax)</div>
                                                    <div className="value">$50</div>
                                                </div>
                                            </div>
                                        </Card.Body>
                                    </Accordion.Collapse>
                                </Card>
                            </Accordion>
                        </div>
                    </Container>
                </div>
                <div className="item">
                    <Container>
                        <div className="inner-item">
                            <Accordion className='default'  defaultActiveKey="0">
                                <Card>
                                    <Accordion.Toggle className='bg-fff pr-0 pl-0' as={Card.Header} eventKey="1">
                                        <div className="fw-500">Shipping Details</div>
                                        <i className="fa fa-angle-down" aria-hidden="true"></i>
                                    </Accordion.Toggle>
                                    <Accordion.Collapse eventKey="1">
                                        <Card.Body>
                                            <div className='fs-14'>content</div>
                                            <div className='fs-14'>content</div>
                                            <div className='fs-14'>content</div>
                                            <div className='fs-14'>content</div>
                                        </Card.Body>
                                    </Accordion.Collapse>
                                </Card>
                            </Accordion>
                        </div>
                    </Container>
                </div>
                <div className="item">
                    <Container>
                        <div className="inner-item">
                            <Accordion className='default'  defaultActiveKey="0">
                                <Card>
                                    <Accordion.Toggle className='bg-fff pr-0 pl-0' as={Card.Header} eventKey="1">
                                        <div className="fw-500">Billing Details</div>
                                        <i className="fa fa-angle-down" aria-hidden="true"></i>
                                    </Accordion.Toggle>
                                    <Accordion.Collapse eventKey="1">
                                        <Card.Body>
                                            <div className='fs-14'>content</div>
                                            <div className='fs-14'>content</div>
                                            <div className='fs-14'>content</div>
                                            <div className='fs-14'>content</div>
                                        </Card.Body>
                                    </Accordion.Collapse>
                                </Card>
                            </Accordion>
                        </div>
                    </Container>
                </div>
                <div className="item">
                    <Container>
                        <div className="inner-item">
                            <Accordion className='default'  defaultActiveKey="0">
                                <Card>
                                    <Accordion.Toggle className='bg-fff pr-0 pl-0' as={Card.Header} eventKey="1">
                                        <div className="fw-500">Shipping Method</div>
                                        <i className="fa fa-angle-down" aria-hidden="true"></i>
                                    </Accordion.Toggle>
                                    <Accordion.Collapse eventKey="1">
                                        <Card.Body>
                                            <div className='fs-14'>content</div>
                                            <div className='fs-14'>content</div>
                                            <div className='fs-14'>content</div>
                                            <div className='fs-14'>content</div>
                                        </Card.Body>
                                    </Accordion.Collapse>
                                </Card>
                            </Accordion>
                        </div>
                    </Container>
                </div>

                <div className="item">
                    <Container>
                        <div className="inner-item">
                            <Accordion className='default'  defaultActiveKey="0">
                                <Card>
                                    <Accordion.Toggle className='bg-fff pr-0 pl-0' as={Card.Header} eventKey="1">
                                        <div className="fw-500">Payment Details</div>
                                        <i className="fa fa-angle-down" aria-hidden="true"></i>
                                    </Accordion.Toggle>
                                    <Accordion.Collapse eventKey="1">
                                        <Card.Body>
                                            <div className='fs-14'>content</div>
                                            <div className='fs-14'>content</div>
                                            <div className='fs-14'>content</div>
                                            <div className='fs-14'>content</div>
                                        </Card.Body>
                                    </Accordion.Collapse>
                                </Card>
                            </Accordion>
                        </div>
                    </Container>
                </div>

            </div>

            <Modal dialogClassName="side filter" show={openSideFilter} onHide={openSidebarFilter}>
                <Modal.Header>
                    <Modal.Title>Sort & Filter</Modal.Title>
                    <Button className="ml-auto clear">Clear All</Button>
                </Modal.Header>
                <Modal.Body>

                    <Accordion className="mt-3" defaultActiveKey="0">
                        <Card>
                            <Accordion.Toggle as={Card.Header} eventKey="1">
                                Sort by:
                                <i className="fa fa-angle-down" aria-hidden="true"></i>
                            </Accordion.Toggle>
                            <Accordion.Collapse eventKey="0">
                                <Card.Body>
                                    <div className="d-flex mb-2 w-100">
                                        <div className="label-left">Size</div>
                                        <div className="value-right"><RadioButton name="sort"/></div>
                                    </div>
                                    <div className="d-flex mb-2 w-100">
                                        <div className="label-left">Color</div>
                                        <div className="value-right"><RadioButton name="sort"/></div>
                                    </div>
                                    <div className="d-flex mb-2 w-100">
                                        <div className="label-left">Category</div>
                                        <div className="value-right"><RadioButton name="sort"/></div>
                                    </div>
                                </Card.Body>
                            </Accordion.Collapse>
                        </Card>
                    </Accordion>
                    <Accordion defaultActiveKey="0">
                        <Card>
                            <Accordion.Toggle as={Card.Header} eventKey="1">
                                Filter by:
                                <i className="fa fa-angle-down" aria-hidden="true"></i>
                            </Accordion.Toggle>
                            <Accordion.Collapse eventKey="0">
                                <Card.Body>
                                    <div className="d-flex mb-2 w-100">
                                        <div className="label-left">Size</div>
                                        <div className="value-right"><RadioButton name="filter"/></div>
                                    </div>
                                    <div className="d-flex mb-2 w-100">
                                        <div className="label-left">Color</div>
                                        <div className="value-right"><RadioButton name="filter"/></div>
                                    </div>
                                    <div className="d-flex mb-2 w-100">
                                        <div className="label-left">Category</div>
                                        <div className="value-right"><RadioButton name="filter"/></div>
                                    </div>
                                </Card.Body>
                            </Accordion.Collapse>
                        </Card>
                    </Accordion>
                </Modal.Body>
                <Modal.Footer>
                    <Button className='w-100'>Done</Button>
                </Modal.Footer>
            </Modal>
        </div>
    )
}
export default MyOrderDetail;
