import React, {useState} from 'react';
import {Link} from 'react-router-dom';
import '../../Style/sidebarModal.scss';
import '../../Style/global.scss';
import './style.scss';
import {Button, Container, Modal, Card, Accordion} from "react-bootstrap";
import ButtonClose from '../../components/Button/ButtonClose';
import CheckboxButton from '../../components/Button/CheckboxButton/index';
import RadioButton from "../../components/Button/RadioButton";

function MyOrder() {

    const [openSideFilter, setOpenSideFilter] = useState(false);

    const openSidebarFilter = () =>{
        setOpenSideFilter(!openSideFilter);
    };

    return(
        <div className="wr-my-order">
            <div className="top-bar-title">
                <Container>
                    <div className="text-center position-relative">
                        My Orders
                        <div className="btn-sort">
                            <Button onClick={openSidebarFilter}>
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M12 3.202l3.839 4.798h-7.678l3.839-4.798zm0-3.202l-8 10h16l-8-10zm8 14h-16l8 10 8-10z"/></svg>
                            </Button>
                        </div>
                    </div>
                </Container>
            </div>
            <div className="items">
                <div className="item">
                    <Container>
                        <div className="inner-item">
                            <Link to={`/my-order-detail`}>
                                <div className="details">
                                    <div className="date">16 Oct 2019</div>
                                    <div className="title">#12345678</div>
                                    <div className="desc">Pending</div>
                                </div>
                            </Link>
                            <div className="price ml-auto">$12,34</div>
                            <i className="fa fa-angle-right" aria-hidden="true"></i>
                        </div>
                    </Container>
                </div>
                <div className="item">
                    <Container>
                        <div className="inner-item">
                            <Link to={`/my-order-detail`}>
                                <div className="details">
                                    <div className="date">16 Oct 2019</div>
                                    <div className="title">#12345678</div>
                                    <div className="desc">Pending</div>
                                </div>
                            </Link>
                            <div className="price ml-auto">$12,34</div>
                            <i className="fa fa-angle-right" aria-hidden="true"></i>
                        </div>
                    </Container>
                </div>
                <div className="item">
                    <Container>
                        <div className="inner-item">
                            <Link to={`/my-order-detail`}>
                                <div className="details">
                                    <div className="date">16 Oct 2019</div>
                                    <div className="title">#12345678</div>
                                    <div className="desc">Pending</div>
                                </div>
                            </Link>
                            <div className="price ml-auto">$12,34</div>
                            <i className="fa fa-angle-right" aria-hidden="true"></i>
                        </div>
                    </Container>
                </div>
                <div className="item">
                    <Container>
                        <div className="inner-item">
                            <Link to={`/my-order-detail`}>
                                <div className="details">
                                    <div className="date">16 Oct 2019</div>
                                    <div className="title">#12345678</div>
                                    <div className="desc">Pending</div>
                                </div>
                            </Link>
                            <div className="price ml-auto">$12,34</div>
                            <i className="fa fa-angle-right" aria-hidden="true"></i>
                        </div>
                    </Container>
                </div>
                <div className="item">
                    <Container>
                        <div className="inner-item">
                            <Link to={`/my-order-detail`}>
                                <div className="details">
                                    <div className="date">16 Oct 2019</div>
                                    <div className="title">#12345678</div>
                                    <div className="desc">Pending</div>
                                </div>
                            </Link>
                            <div className="price ml-auto">$12,34</div>
                            <i className="fa fa-angle-right" aria-hidden="true"></i>
                        </div>
                    </Container>
                </div>
                <div className="item">
                    <Container>
                        <div className="inner-item">
                            <Link to={`/my-order-detail`}>
                                <div className="details">
                                    <div className="date">16 Oct 2019</div>
                                    <div className="title">#12345678</div>
                                    <div className="desc">Pending</div>
                                </div>
                            </Link>
                            <div className="price ml-auto">$12,34</div>
                            <i className="fa fa-angle-right" aria-hidden="true"></i>
                        </div>
                    </Container>
                </div>
                <div className="item">
                    <Container>
                        <div className="inner-item">
                            <Link to={`/my-order-detail`}>
                                <div className="details">
                                    <div className="date">16 Oct 2019</div>
                                    <div className="title">#12345678</div>
                                    <div className="desc">Pending</div>
                                </div>
                            </Link>
                            <div className="price ml-auto">$12,34</div>
                            <i className="fa fa-angle-right" aria-hidden="true"></i>
                        </div>
                    </Container>
                </div>
                <div className="item">
                    <Container>
                        <div className="inner-item">
                            <Link to={`/my-order-detail`}>
                                <div className="details">
                                    <div className="date">16 Oct 2019</div>
                                    <div className="title">#12345678</div>
                                    <div className="desc">Pending</div>
                                </div>
                            </Link>
                            <div className="price ml-auto">$12,34</div>
                            <i className="fa fa-angle-right" aria-hidden="true"></i>
                        </div>
                    </Container>
                </div>
                <div className="item">
                    <Container>
                        <div className="inner-item">
                            <Link to={`/my-order-detail`}>
                                <div className="details">
                                    <div className="date">16 Oct 2019</div>
                                    <div className="title">#12345678</div>
                                    <div className="desc">Pending</div>
                                </div>
                            </Link>
                            <div className="price ml-auto">$12,34</div>
                            <i className="fa fa-angle-right" aria-hidden="true"></i>
                        </div>
                    </Container>
                </div>
            </div>

            <Modal dialogClassName="side filter" show={openSideFilter} onHide={openSidebarFilter}>
                <Modal.Header>
                    <Modal.Title>Sort & Filter</Modal.Title>
                    <Button className="ml-auto clear">Clear All</Button>
                </Modal.Header>
                <Modal.Body>

                    <Accordion className="mt-3" defaultActiveKey="0">
                        <Card>
                            <Accordion.Toggle as={Card.Header} eventKey="1">
                                Sort by:
                                <i className="fa fa-angle-down" aria-hidden="true"></i>
                            </Accordion.Toggle>
                            <Accordion.Collapse eventKey="0">
                                <Card.Body>
                                    <div className="d-flex mb-2 w-100">
                                        <div className="label-left">Size</div>
                                        <div className="value-right"><RadioButton name="sort"/></div>
                                    </div>
                                    <div className="d-flex mb-2 w-100">
                                        <div className="label-left">Color</div>
                                        <div className="value-right"><RadioButton name="sort"/></div>
                                    </div>
                                    <div className="d-flex mb-2 w-100">
                                        <div className="label-left">Category</div>
                                        <div className="value-right"><RadioButton name="sort"/></div>
                                    </div>
                                </Card.Body>
                            </Accordion.Collapse>
                        </Card>
                    </Accordion>
                    <Accordion defaultActiveKey="0">
                        <Card>
                            <Accordion.Toggle as={Card.Header} eventKey="1">
                                Filter by:
                                <i className="fa fa-angle-down" aria-hidden="true"></i>
                            </Accordion.Toggle>
                            <Accordion.Collapse eventKey="0">
                                <Card.Body>
                                    <div className="d-flex mb-2 w-100">
                                        <div className="label-left">Size</div>
                                        <div className="value-right"><RadioButton name="filter"/></div>
                                    </div>
                                    <div className="d-flex mb-2 w-100">
                                        <div className="label-left">Color</div>
                                        <div className="value-right"><RadioButton name="filter"/></div>
                                    </div>
                                    <div className="d-flex mb-2 w-100">
                                        <div className="label-left">Category</div>
                                        <div className="value-right"><RadioButton name="filter"/></div>
                                    </div>
                                </Card.Body>
                            </Accordion.Collapse>
                        </Card>
                    </Accordion>
                </Modal.Body>
                <Modal.Footer>
                    <Button className='w-100'>Done</Button>
                </Modal.Footer>
            </Modal>
        </div>
    )
}
export default MyOrder;
