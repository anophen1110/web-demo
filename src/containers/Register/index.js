import React, {useState} from 'react';
import useForm from 'react-hook-form';
import {Button, Container, Form, Image,Modal} from "react-bootstrap";
import './style.scss';
import '../../Style/global.scss';
import InputWrapLabel from "../../components/Input/InputWrapLabel";
import CheckboxButton from "../../components/Button/CheckboxButton";
import ic_password from "../../Assets/assets/ic_password.png";
import ic_password_hide from "../../Assets/assets/ic_password_hide.png";
import {SpanEye} from "../DevLogin/StyledDevLogin";

function Register() {
    const methods = useForm();
    const [isShowP, setIsShowP] = useState(true);
    const [isShowConfirmP, setIsShowConfirmP] = useState(true);
    const [gender, setGender] = useState(true);
    const [customerType, setCustomerType] = useState(true);
    const [showDisclaimer, setShowDisclaimer] = useState(false);
    const [showTerm, setShowTerm] = useState(false);

    const isShowPassword = () =>{
        setIsShowP(!isShowP);
    };

    const isShowConfirmPassword = () =>{
        setIsShowConfirmP(!isShowConfirmP);
    };

    return(
        <div className="wr-register">
            <div className="top-bar-title">Create New Account</div>
            <div className="text-alert-required">* Please fill in all required fields</div>
            <div className="wr-content">
                <Form>
                    <div className="wr-inner-content">
                        <Container>
                            <div className="row-gender">
                                <div className="label">Gender</div>
                                <label className="value ml-auto">
                                    <input type="radio" name='gender'/>
                                        <span className="checkmark">Unspecified</span>
                                </label>
                                <label className="value ml-2" >
                                    <input type="radio" name='gender'/>
                                        <span className="checkmark">Male</span>
                                </label>
                                <label className="value ml-2">
                                    <input type="radio" name='gender'/>
                                        <span className="checkmark">Female</span>
                                </label>
                            </div>
                            <div className='wr-input'>
                                <InputWrapLabel type='text' name='firstName' classinput="input-no-border" title='FIRST NAME*'/>
                            </div>
                            <div className='wr-input'>
                                <InputWrapLabel type='text' name='middleName' classinput="input-no-border" title='MIDDLE NAME'/>
                            </div>
                            <div className='wr-input'>
                                <InputWrapLabel type='text' name='lastName' classinput="input-no-border" title='LAST NAME*'/>
                            </div>
                            <div className='wr-input'>
                                <InputWrapLabel type='mail' name='mail' classinput="input-no-border" title='EMAIL ADDRESS*'/>
                            </div>
                            <div className='wr-input'>
                                <InputWrapLabel type='number' name='phone' classinput="input-no-border" title='TELEPHONE NUMBER'/>
                            </div>
                            <div className='wr-input'>
                                <InputWrapLabel type='number' name='mobile' classinput="input-no-border" title='MOBILE NUMBER*'/>
                            </div>
                            <div className='wr-input'>
                                <InputWrapLabel type='date' name='DoB' classinput="input-no-border" title='DATE OF BIRTH*'/>
                            </div>
                            <div className='wr-input'>
                                <InputWrapLabel type='text' name='NIN' classinput="input-no-border" title='National Identification Number'/>
                            </div>
                        </Container>
                    </div>
                    <div className="wr-inner-content">
                        <Container>
                            <div className='wr-input position-relative'>
                                <InputWrapLabel type={isShowP?"password":"text"} name='password' classinput="input-no-border" title='Password*'/>
                                <SpanEye onClick={isShowPassword}><Image src={isShowP?ic_password:ic_password_hide} /></SpanEye>
                            </div>
                            <div className='wr-input position-relative'>
                                <InputWrapLabel type={isShowConfirmP?"password":"text"} name='confirmPassword' classinput="input-no-border" title='Confirm Password*'/>
                                <SpanEye onClick={isShowConfirmPassword}><Image src={isShowConfirmP?ic_password:ic_password_hide} /></SpanEye>
                            </div>
                        </Container>
                    </div>
                    <div className="wr-inner-content">
                        <Container>
                            <div className="row-gender">
                                <div className="label">Customer Type*</div>
                                <div className="value active ml-auto">Private</div>
                                <div className="value ml-2">Business</div>
                            </div>

                            <div className='wr-input'>
                                <InputWrapLabel type='text' name='companyName' classinput="input-no-border" title='Company Name*'/>
                            </div>
                            <div className='wr-input'>
                                <InputWrapLabel type='number' name='COCNumber' classinput="input-no-border" title='COC Number'/>
                            </div>
                            <div className='wr-input'>
                                <InputWrapLabel type='text' name='vatTax' classinput="input-no-border" title='VAT/Tax Number'/>
                            </div>
                        </Container>
                    </div>
                    <div className="wr-bar-submit">
                        <Container>
                            <div className="wr-terms-conditions">
                                <div className="label">TERMS & CONDITIONS</div>
                                <div className="gr-radio-label">
                                    <CheckboxButton className='square-box-blue' name='confirm'/>
                                    <span>By registering an account, I accept the <span onClick={()=>setShowTerm(true)} className='link'>general terms & conditions</span> and confirm that I have read the <span onClick={()=>setShowDisclaimer(true)} className='link'>disclaimer</span>.</span>
                                </div>
                            </div>
                            <div className="wr-btn-submit">
                                <Button type='submit'>Submit</Button>
                            </div>
                        </Container>
                    </div>
                </Form>
            </div>
            <Modal size='lg' dialogClassName={'register'} show={showDisclaimer} onHide={()=>setShowDisclaimer(false)}>
                <Modal.Header closeButton>
                    <div className="title">Disclaimer</div>
                </Modal.Header>
                <Modal.Body>
                    <p>Advertising is telling the world how great you are, while publicity is having others tell the world how great you are. Companies and organizations that understand the importance of publicity are generally more successful than those who use advertising alone to reach their target audiences.</p>
                    <p>Newspapers, magazines and radio and television news programs are filled with stories about businesses and organizations. Those who know how to present materials professionally to the right people will get media coverage for free, while those who don’t understand how publicity works have to rely on large advertising budgets to reach their audiences, usually with less impressive results.</p>
                    <p>“If you can get a news organization to report about your event or praise something you’re doing, you have gained an enormous amount of credibility that simply couldn’t be generated through paid advertising,” said David Forman, a public relations veteran and author of “Publicity Professor,” a workbook that teaches business owners how to get free news coverage.</p>
                    <p>The first step in reaching out to media professionals is to create an appealing press release. Its main purpose is to get an editor or producer interested in what you have to say or in what you are doing. It should contain enough information, specifics</p>
                </Modal.Body>
            </Modal>

            <Modal dialogClassName={'register'} size='lg' show={showTerm} onHide={()=> setShowTerm(false)}>
                <Modal.Header closeButton>
                    <div className="title">General Terms & Conditions</div>
                </Modal.Header>
                <Modal.Body>
                    <p>Advertising is telling the world how great you are, while publicity is having others tell the world how great you are. Companies and organizations that understand the importance of publicity are generally more successful than those who use advertising alone to reach their target audiences.</p>
                    <p>Newspapers, magazines and radio and television news programs are filled with stories about businesses and organizations. Those who know how to present materials professionally to the right people will get media coverage for free, while those who don’t understand how publicity works have to rely on large advertising budgets to reach their audiences, usually with less impressive results.</p>
                    <p>“If you can get a news organization to report about your event or praise something you’re doing, you have gained an enormous amount of credibility that simply couldn’t be generated through paid advertising,” said David Forman, a public relations veteran and author of “Publicity Professor,” a workbook that teaches business owners how to get free news coverage.</p>
                    <p>The first step in reaching out to media professionals is to create an appealing press release. Its main purpose is to get an editor or producer interested in what you have to say or in what you are doing. It should contain enough information, specifics</p>
                </Modal.Body>
            </Modal>
        </div>
    )
}
export default Register;
