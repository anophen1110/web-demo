import Lodash from "lodash";
import { sprintf, vsprintf } from "sprintf-js";
import htmlTags from "html-tags";
import { NumberFormat } from "intl";
import * as Constants from "../Constants";
import Moment from "moment";


const { CurrencyKey } = Constants;

export const convertTimeStamp = unixTimeStr => {
  const timeSent = safeParseFloat(unixTimeStr);
  let dateStr = Moment.unix(timeSent / 1000).format(
    "dddd, MMMM Do, YYYY - h:mm A"
  );

  return dateStr;
};

export const removeHtmlTag = content => {
  const regex = /(<([^>]+)>)/gi;
  return content.replace(regex, "");
};

export const convertNumberWithDot = num => {
  let parts = num.toString().split(".");
  parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ".");
  return parts.join(".");
};



export const getDistanceMeter = (lat1, lon1, lat2, lon2) => {
  let R = 6371; // Radius of the earth in km
  let dLat = deg2rad(lat2 - lat1); // deg2rad below
  let dLon = deg2rad(lon2 - lon1);
  let a =
    Math.sin(dLat / 2) * Math.sin(dLat / 2) +
    Math.cos(deg2rad(lat1)) *
      Math.cos(deg2rad(lat2)) *
      Math.sin(dLon / 2) *
      Math.sin(dLon / 2);
  let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  let d = Lodash.round(R * c * 1000); // Distance in meter
  return d;
};

export const deg2rad = deg => {
  return deg * (Math.PI / 180);
};


export const truncateTextAnalytic = text => {
  let validText = "";

  validText = Lodash.truncate(text, {
    length: 100,
    separator: ""
  });

  return validText;
};

export const getSizeImg = uriImg => {
  return new Promise((resolve, reject) => {
    Image.getSize(
      uriImg,
      (width, height) => {
        resolve({ widthImg: width, heightImg: height });
      },
      err => resolve({ widthImg: 0, heightImg: 0 })
    );
  });
};

export const getAllEcomCategory = (
  array,
  keyGet,
  idModuleParent,
  resultArray = []
) => {
  Lodash.forEach(array, function(obj) {
    let listChild = getSafeValue(obj, keyGet, []);
    const name = getSafeValue(obj, "name", "");
    // console.log("object child", obj);
    const newObj = { name, idModuleParent, objBrowseList: obj };
    resultArray.push(newObj);
    if (listChild.length > 0) {
      getAllEcomCategory(listChild, keyGet, idModuleParent, resultArray);
    }
  });
  return resultArray;
};

export const isHtmlStr = str => {
  const basic = /\s?<!doctype html>|(<html\b[^>]*>|<body\b[^>]*>|<x-[^>]+>)+/i;
  const full = new RegExp(
    htmlTags.map(tag => `<${tag}\\b[^>]*>`).join("|"),
    "i"
  );
  return basic.test(str) || full.test(str);
};

export const isValidColor = strColor => {
  return /(^#[0-9A-F]{6}$)|(^#[0-9A-F]{3}$)/i.test(strColor);
};

export const safeParseInt = strNumber => {
  let numParse = 0;
  if (strNumber === "" || strNumber === null || strNumber === undefined) {
    return numParse;
  }

  numParse = Lodash.parseInt(strNumber);
  if (numParse === null || Lodash.isNaN(numParse)) {
    numParse = 0;
  }

  return numParse;
};

export const safeParseFloat = strNumber => {
  let numParse = 0;
  if (strNumber === "" || strNumber === null || strNumber === undefined) {
    return numParse;
  }

  numParse = parseFloat(strNumber);
  if (numParse === null) {
    numParse = 0;
  }

  return numParse;
};

export const getDeviceLocale = currencyCode => {
  if (currencyCode.toUpperCase() === CurrencyKey.KEY_EUR.toUpperCase()) {
    return "nl-NL";
  } else if (currencyCode.toUpperCase() === CurrencyKey.KEY_SAR.toUpperCase()) {
    return "en-US";
  } else if (currencyCode.toUpperCase() === CurrencyKey.KEY_SEK.toUpperCase()) {
    return "sv-SE";
  } else if (currencyCode.toUpperCase() === CurrencyKey.KEY_USD.toUpperCase()) {
    return "en-US";
  } else {
    return "en-US";
  }
};

export const convertCurrency = (value, currencyCode) => {
  const val = safeParseFloat(value);
  const valueRound = Math.round(val * 100) / 100;
  const currencyDefault = getSafeValue(
    // GlobalSettingApp.settingApp,
    "currency",
    CurrencyKey.KEY_USD
  );

  const locale = getDeviceLocale(currencyCode || currencyDefault);
  // const testVal = new NumberFormat("en-AU", {
  //   style: "currency",
  //   currency: "AUD"
  // }).format(valueRound);
  // console.log("testVal", testVal);

  return new NumberFormat(locale, {
    style: "currency",
    currency: currencyCode || currencyDefault,
    minimumFractionDigits: 0,
    maximumFractionDigits: 2
  }).format(valueRound);
};



export const safeParseJson = jsonValue => {
  let objParsed = {};
  if (
    jsonValue &&
    jsonValue !== "" &&
    typeof jsonValue === "string" &&
    jsonValue !== null
  ) {
    objParsed = JSON.parse(jsonValue);
  }
  return Lodash.isObject(objParsed) ? objParsed : {};
};

export const safeParseListJson = jsonValue => {
  let objParsed = [];
  if (
    jsonValue &&
    jsonValue !== "" &&
    typeof jsonValue === "string" &&
    jsonValue !== null
  ) {
    objParsed = JSON.parse(jsonValue);
  }

  return Lodash.isArray(objParsed) ? objParsed : [];
};

export const replaceStrUrl = (baseUrl, arrStr) => {
  let path = vsprintf(baseUrl, arrStr);
  return path;
};

export const getSafeValue = (object, keyItem, defaultValue) => {
  let safeValue = Lodash.get(object, keyItem, defaultValue);
  if (safeValue === null) {
    safeValue = defaultValue;
  }

  if (safeValue === "") {
    safeValue = defaultValue;
  }

  if (
    safeValue !== null &&
    defaultValue !== null &&
    (typeof safeValue !== typeof defaultValue ||
      safeValue.constructor !== defaultValue.constructor)
  ) {
    safeValue = defaultValue;
  }

  // console.log("safeValue", safeValue);

  return safeValue;
};

export const isCloseToBottom = (
  { layoutMeasurement, contentOffset, contentSize },
  offsetList
) => {
  let paddingToBottom = 10;
  let isScrollDown = true;
  const currentOffset = contentOffset.y;
  const dif = currentOffset - (offsetList || 0);
  const isLongeContent = contentSize.height > layoutMeasurement.height;

  if (Math.abs(dif) < 3) {
    // console.log("unclear");
    isScrollDown = false;
  } else if (dif < 0) {
    // console.log("up");
    isScrollDown = false;
  } else {
    // console.log("down");
    isScrollDown = true;
  }

  offsetList = currentOffset;
  let isCloseToBot =
    layoutMeasurement.height + contentOffset.y >=
    contentSize.height - paddingToBottom;
  return isCloseToBot && isScrollDown && isLongeContent;
};
