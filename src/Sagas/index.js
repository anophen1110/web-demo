import { put, takeEvery, call, all, fork } from "redux-saga/effects";
import callApiSaga from "./callApiSaga";

export default function* rootSaga() {
  yield all([
    callApiSaga(),
  ]);
}
