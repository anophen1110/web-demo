import produce from 'immer';
export const SHOW_ALERT = "SHOW_ALERT";

export const initialState ={
  massage:''
};
export function reducer (state = initialState, action){
    produce(state, (draft)=>{
        switch (action.type) {
            case SHOW_ALERT:
                draft.errorMessage = action.message;
                break;
            default:
                return state;
        }
    });
}


export default reducer;
