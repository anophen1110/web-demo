export const baseUrl = "https://merkel-api.jmango360.com";
export const amazonUrl =
  "http://ec2-18-139-253-166.ap-southeast-1.compute.amazonaws.com:3023";
export const getListApp = "/common/apps/";
export const getListLanguage = "/common/apps/getAssociated/%s";
export const loginUser = "/common/so-login";
export const googleLogin = "/common/google-plus/so-login";
export const facebookLogin = "/common/facebook/so-login";
export const registerApp = "/common/%s/register";
export const syncApp = "/common/%s/sync";
export const getProductsModule = "/ecom/%s/products";
export const getListCart =
  "ecom/%s/carts/cart?appKey=%s&deviceKey=%s&ticket=%s";
export const deleteCartItem = "ecom/%s/carts/cart/%s/items/item/%s";
export const editquanityCartItem = "ecom/%s/carts/cart/%s/items/item/%s/qty";
export const applyCouponCart = "ecom/%s/carts/cart/%s/coupon";
export const removeCouponCart = "ecom/%s/carts/cart/%s/coupon/%s";
export const getCrossSell =
  "ecom/%s/products/%s/cross_sell?appKey=%s&deviceKey=%s&ticket=%s";
export const getCartCount =
  "ecom/%s/carts/cart/count?appKey=%s&deviceKey=%s&ticket=%s";
export const addItemToNewCart = "/ecom/%s/carts/cart";
export const addItemIntoExistingCart = "ecom/%s/carts/cart/%s/items";
export const updateItemIntoExistingCart =
  "/ecom/%s/carts/cart/%s/items/item/%s";
export const loginMagento = "ecom/%s/accounts/login";
export const forgetPass = "ecom/%s/accounts/forgot-password";
export const registerMagento = "ecom/%s/accounts/register-extra";
export const createUserAddress = "ecom/%s/accounts/%s/addresses";
export const getUserAddress =
  "ecom/%s/accounts/%s/addresses?deviceKey=%s&appKey=%s";
export const editUserAddress = "ecom/%s/accounts/%s/addresses";
export const setCartAddress = "ecom/%s/carts/cart/%s/addresses";
export const setCartShippingMethod = "ecom/%s/carts/cart/%s/shipping";
export const getTokenPayment = "ecom/%s/carts/payments/in-app/tokens";
export const updatePaymentComplete = "ecom/%s/carts/cart/%s/complete";
export const autoLogin =
  "/ecom/%s/autologin/%s/%s/?url=%s&keypairData=%s&keypairVersion=%s";
export const getRegionList = "ecom/%s/region/list";
export const onePageCheckOut = "/ecom/%s/payment/webcheckout/%s/%s";
export const userLogOut = "ecom/%s/accounts/logout";
export const editProfilt = "ecom/%s/accounts/update";
export const reloadProfile = "ecom/%s/accounts/%s/?appKey=%s&deviceKey=%s";
export const deleteUserAddress =
  "ecom/%s/accounts/%s/addresses?appKey=%s&deviceKey=%s&ticket=%s&customerId=%s&addressId=%s";
export const getWishList = "ecom/%s/accounts/wishlist/get";
export const updateWishList = "ecom/%s/accounts/wishlist/update";
export const updateProductInWatchList =
  "/ecom/%s/accounts/wishlist/updateoptions";
export const removeWishList = "ecom/%s/accounts/wishlist/remove";
export const getOrderList =
  "ecom/%s/accounts/%s/orders?appKey=%s&deviceKey=%s&pageSize=%s&pageNumber=%s";
export const getOrderDetails =
  "ecom/%s/accounts/%s/orders/%s/?appKey=%s&deviceKey=%s";
export const sendEnquiry = "ecom/%s/enquiry/submit";
export const associateSourceStripe = "ecom/%s/payment/stripe/sources/associate";
export const submitChargesStripe = "ecom/%s/payment/stripe/charges/submit";
export const initPaymentIntent =
  "ecom/%s/payment/stripe/auto/payment-intent/init";
export const completeOrderStripe = "ecom/%s/carts/completeOrder";
export const DEV_REGISTER_SERVER =
  "https://dev-register.jmango360.com/endpoints";
export const PRO_REGISTER_SERVER = "https://register.jmango360.com/endpoints";
export const searchSuggestion = "ecom/%s/searchsuggestion";
export const listProductSearch = "ecom/%s/products/search";
export const validatePayment = "ecom/%s/carts/validatePayment";
// API Detail product.
export const detailProduct = "/ecom/%s/products/details";
export const reviewDetailProduct =
  "/ecom/%s/products/%s/reviews?appKey=%s&deviceKey=%s";
export const configReviewDetailProduct =
  "/ecom/%s/products/%s/reviews/config?appKey=%s&deviceKey=%s";
export const relatedProduct =
  "/ecom/%s/products/%s/related?appKey=%s&deviceKey=%s&ticket=%s";
export const upSellProduct =
  "/ecom/%s/products/%s/up_sell?appKey=%s&deviceKey=%s&ticket=%s";
export const addReviewProduct = "/ecom/%s/products/%s/reviews";
export const addProductToWatchList = "/ecom/%s/accounts/wishlist/add";
export const removeProductFromWatchList = "/ecom/%s/accounts/wishlist/remove";
// get price sale of config_scp product after finish picking all option
export const getPriceSaleProduct = "/ecom/%s/products/%s/reload";

//API Push message
export const registerPushNotifi = "/common/%s/register-push";
