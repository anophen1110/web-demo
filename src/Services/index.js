import APISauce from "apisauce";
import Lodash from "lodash";
import * as Utils from "../utils";
import { HeaderKey } from "../Constants";

const uuidv4 = require("uuid/v4");



export const fingerPrint = () => {
  return {
    fingerprint: uuidv4(),
    model: "",
    version: "",
    os: "Android OS",
    manufacturer: ""
  };
};
