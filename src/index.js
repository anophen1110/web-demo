import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'font-awesome/css/font-awesome.min.css';
import { Provider } from "react-redux";
import App from './containers/App/App';
import * as ServiceWorker from './containers/App/serviceWorker';
import store, { persistorStore, reducerNameHOR } from "./redux";

const MOUNT_NODE = document.getElementById('root');
    ReactDOM.render(
        <Provider store={store}>
            <App />
        </Provider>
        , MOUNT_NODE
    );

ServiceWorker.unregister();
