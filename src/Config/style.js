import { StyleSheet, Dimensions } from "react-native";

const masterStyles = StyleSheet.create({
  rowContainer: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between"
    // alignItems: 'center',
  },

  rowStartContainer: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "flex-start"
    // alignItems: 'center',
  },

  rowEndContainer: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "flex-end"
    // alignItems: 'center',
  },

  columnContainer: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "space-between"
    // alignItems: 'center',
  },

  leftContainer: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "flex-start",
    alignItems: "flex-start"
  },

  rightContainer: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "flex-end",
    alignItems: "flex-end"
  },

  leftCenterContainer: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "flex-start"
  },

  rightCenterContainer: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "flex-end"
  },

  leftTopContainer: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "flex-start",
    alignItems: "flex-start"
  },

  rightTopContainer: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "flex-start",
    alignItems: "flex-end"
  },

  leftRowTopContainer: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center"
  },

  rightRowTopContainer: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "flex-end"
  },

  viewColumn: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center"
  },

  viewRow: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center"
  }
});

export default masterStyles;
