import React, { Component } from "react";
import { Platform, Easing, Animated, Dimensions } from "react-native";
import {
  createStackNavigator,
  createSwitchNavigator,
  createBottomTabNavigator,
  createDrawerNavigator,
  withNavigation,
  createAppContainer,
  StackViewTransitionConfigs,
  NavigationActions
} from "react-navigation";
import createAnimatedSwitchNavigator from "../Utils/AnimatedSwitchNavigator";

import {
  FluidNavigator,
  createFluidNavigator
} from "react-navigation-fluid-transitions";
/**
 * Component import
 */
import HomeScreen from "../Containers/Home";
import SearchProductScreen from "../Containers/SearchProduct";
import WatchListScreen from "../Containers/WatchList";
import MoreScreen from "../Containers/More";
import DevLogin from "../Containers/DevLogin";
import ListApp from "../Containers/ListApp";
import ListLanguageSupport from "../Containers/ListLanguageSupport";
import ShoppingCart from "../Containers/ShoppingCart";
import SplashLoading from "../Containers/SplashLoading";
import EcommerceCatalog from "../Containers/EcommerceCatalog";
import MagentoLogin from "../Containers/MagentoLogin";
import ForgetPassWord from "../Containers/ForgetPassWord";
import ListProductSearch from "../Containers/ListProductSearch";
import ProductDetail from "../Containers/ProductDetail";
import ProductDetailImage from "../Containers/ProductDetail/ProductDetailImage";

import BottomTabBar from "../Components/BottomTabBar";

import Colors from "./Colors";
import Fonts from "./Fonts";
import * as Utils from "../Utils";
import * as TransitionConfig from "../Utils/TransitionConfig";

import FluidAnim from "../../TestCom/FluidAnim";
import MagentoRegister from "../Containers/MagentoRegister";
import RegisterWebView from "../Containers/RegisterWebView";
import CheckOut from "../Containers/CheckOut";
import NewAddress from "../Containers/CheckOut/NewAddress";
import GeneralTerm from "../Containers/GeneralTerm";
import WebCheckout from "../Containers/WebCheckOut";
import ThankYouPage from "../Containers/ThankYouPage";
import MyAccount from "../Containers/MyAccount";
import EditProfile from "../Containers/EditProfile";
import UserAddress from "../Containers/UserAddress";
import MyOrder from "../Containers/MyOrder";
import AddUserAddress from "../Containers/UserAddress/AddUserAddress";
import AddUserAddressWebView from "../Containers/UserAddress/AddUserAddressWebView";
import WishListNote from "../Containers/WatchList/WishListNote";
import ListReviewProduct from "../Containers/ListReviewProduct";
import AddReviewProduct from "../Containers/AddReviewProduct";
import OrderDetails from "../Containers/MyOrder/OrderDetails";
import ProductOrderDetails from "../Containers/MyOrder/ProductOrderDetails";
import { HomeModule } from "./HomeModule";
import InitHomeScreen from "../Containers/InitHomeScreen";
import InfoPage from "../Containers/InfoPage";
import Enquiry from "../Containers/Enquiry";
import LookBook from "../Containers/LookBook";
import ListLookBook from "../Containers/ListLookBook";
import WebViewHome from "../Containers/Home/WebViewHome";
import Location from "../Containers/Location";
import LocationDetail from "../Containers/LocationDetail";
import MapGuildUser from "../Containers/MapGuildUser";
import ScanBarcode from "../Containers/ScanBarcode";
import ListNotification from "../Containers/ListNotification";
import SourceParamComponent from "../Containers/CheckOut/Stripe/SourceParamComponent";
import DetailNotification from "../Containers/DetailNotification";
import MapCurrentStore from "../Containers/LocationDetail/MapCurrentStore";

const isIOS = Platform.OS === "ios" ? true : false;
const { width, height } = Dimensions.get("window");
export const RoutesName = {
  //Screen Name.
  HomeScreen: "HomeScreen",
  SearchProductScreen: "SearchProductScreen",
  WatchListScreen: "WatchListScreen",
  ShoppingCart: "ShoppingCart",
  MoreScreen: "MoreScreen",
  DevLogin: "DevLogin",
  ListApp: "ListApp",
  ListLanguageSupport: "ListLanguageSupport",
  SplashLoading: "SplashLoading",
  EcommerceCatalog: "EcommerceCatalog",
  MagentoLogin: "MagentoLogin",
  ForgetPassWord: "ForgetPassWord",
  MagentoRegister: "MagentoRegister",
  RegisterWebView: "RegisterWebView",
  ListProductSearch: "ListProductSearch",
  CheckOut: "CheckOut",
  EditAddress: "EditAddress",
  ProductDetail: "ProductDetail",
  ProductDetailImage: "ProductDetailImage",
  ProductDetailRoot: "ProductDetailRoot",
  GeneralTerm: "GeneralTerm",
  WebCheckOut: "WebCheckOut",
  ThankYouPage: "ThankYouPage",
  MyAccount: "MyAccount",
  EditProfile: "EditProfile",
  ListReviewProduct: "ListReviewProduct",
  AddReviewProduct: "AddReviewProduct",
  LookBook: "LookBook",
  ListLookBook: "ListLookBook",
  Location: "Location",
  LocationDetail: "LocationDetail",
  MapGuildUser: "MapGuildUser",
  ScanBarcode: "ScanBarcode",
  ListNotification: "ListNotification",
  DetailNotification: "DetailNotification",
  MapCurrentStore: "MapCurrentStore",
  //Stack Name
  HomeStack: "HomeStack",
  SearchStack: "SearchStack",
  WatchListStack: "WatchListStack",
  ShopBagStack: "ShopBagStack",
  MoreStack: "MoreStack",
  RootStack: "RootStack",
  LoginStack: "LoginStack",
  TabNav: "TabNav",
  DrawerNav: "DrawerNav",
  AppStack: "AppStack",
  UserAddress: "UserAddress",
  MyOrder: "MyOrder",
  AddUserAddress: "AddUserAddress",
  AddUserAddressWebView: "AddUserAddressWebView",
  WishListNote: "WishListNote",
  OrderDetails: "OrderDetails",
  ProductOrderDetails: "ProductOrderDetails",
  InitHomeScreen: "InitHomeScreen",
  InfoPage: "InfoPage",
  Enquiry: "Enquiry",
  WebViewHome: "WebViewHome",
  SourceParamComponent: "SourceParamComponent"
};

const HomeStack = createStackNavigator(
  {
    [RoutesName.InitHomeScreen]: {
      screen: InitHomeScreen
    },
    [RoutesName.SearchProductScreen]: {
      screen: SearchProductScreen
    },
    [RoutesName.HomeScreen]: {
      screen: HomeScreen
    },
    [RoutesName.InfoPage]: {
      screen: InfoPage
    },
    [RoutesName.Enquiry]: {
      screen: Enquiry
    },
    [RoutesName.EcommerceCatalog]: {
      screen: EcommerceCatalog
    },
    [RoutesName.LookBook]: {
      screen: LookBook
    },
    [RoutesName.ListLookBook]: {
      screen: ListLookBook
    },
    [RoutesName.WebViewHome]: {
      screen: WebViewHome
    },
    [RoutesName.Location]: {
      screen: Location
    },
    [RoutesName.ListProductSearch]: {
      screen: ListProductSearch
    },
    [RoutesName.ProductDetail]: {
      screen: ProductDetail
    },
    [RoutesName.EcommerceCatalog]: {
      screen: EcommerceCatalog
    },
    [RoutesName.ScanBarcode]: {
      screen: ScanBarcode
    }
  },
  {
    // initialRouteName: RoutesName.Enquiry,
    headerMode: "none",
    defaultNavigationOptions: {
      gesturesEnabled: false
    }
  }
);

const handleCustomTransition = ({ scenes }) => {
  const prevScene = scenes[scenes.length - 2];
  const nextScene = scenes[scenes.length - 1];

  if (prevScene && nextScene.route.routeName === RoutesName.ScanBarcode) {
    return TransitionConfig.fromBottom();
  }

  return StackViewTransitionConfigs.defaultTransitionConfig;
};

const SearchStack = createStackNavigator(
  {
    [RoutesName.SearchProductScreen]: {
      screen: SearchProductScreen
    },
    [RoutesName.ListProductSearch]: {
      screen: ListProductSearch
    },
    [RoutesName.ProductDetail]: {
      screen: ProductDetail
    },
    [RoutesName.EcommerceCatalog]: {
      screen: EcommerceCatalog
    },
    [RoutesName.ScanBarcode]: {
      screen: ScanBarcode
    }
  },
  {
    headerMode: "none",
    transitionConfig: nav => handleCustomTransition(nav),
    defaultNavigationOptions: {
      gesturesEnabled: false
    }
  }
);

SearchStack.navigationOptions = ({ navigation }) => {
  const indexRoute = navigation.state.index;
  const tabBarVisible = Utils.getSafeValue(
    navigation.state.routes[indexRoute],
    "params.isShowTabBar",
    true
  );

  return {
    tabBarVisible
  };
};

const WatchListStack = createStackNavigator(
  {
    [RoutesName.WatchListScreen]: {
      screen: WatchListScreen
    },
    [RoutesName.WishListNote]: {
      screen: WishListNote
    },
    [RoutesName.ProductDetail]: {
      screen: ProductDetail
    },
    [RoutesName.WebViewHome]: {
      screen: WebViewHome
    },
    [RoutesName.ListReviewProduct]: {
      screen: ListReviewProduct
    },
    [RoutesName.AddReviewProduct]: {
      screen: AddReviewProduct
    }
  },
  {
    headerMode: "none",
    defaultNavigationOptions: {
      gesturesEnabled: false
    }
  }
);

const ShopBagStack = createStackNavigator(
  {
    [RoutesName.ShoppingCart]: {
      screen: ShoppingCart
    },
    [RoutesName.ProductDetail]: {
      screen: ProductDetail
    },
    [RoutesName.WebViewHome]: {
      screen: WebViewHome
    },
    [RoutesName.ListReviewProduct]: {
      screen: ListReviewProduct
    },
    [RoutesName.AddReviewProduct]: {
      screen: AddReviewProduct
    }
  },
  {
    headerMode: "none",
    defaultNavigationOptions: {
      gesturesEnabled: false
    }
  }
);

const MoreStack = createStackNavigator(
  {
    [RoutesName.MoreScreen]: {
      screen: MoreScreen
    },
    [RoutesName.EcommerceCatalog]: {
      screen: EcommerceCatalog
    },
    [RoutesName.ProductDetail]: {
      screen: ProductDetail
    },
    [RoutesName.MyAccount]: {
      screen: MyAccount
    },
    [RoutesName.EditProfile]: {
      screen: EditProfile
    },
    [RoutesName.UserAddress]: {
      screen: UserAddress
    },
    [RoutesName.MyOrder]: {
      screen: MyOrder
    },
    [RoutesName.AddUserAddress]: {
      screen: AddUserAddress
    },
    [RoutesName.AddUserAddressWebView]: {
      screen: AddUserAddressWebView
    },
    [RoutesName.ListReviewProduct]: {
      screen: ListReviewProduct
    },
    [RoutesName.AddReviewProduct]: {
      screen: AddReviewProduct
    },
    [RoutesName.OrderDetails]: {
      screen: OrderDetails
    },
    [RoutesName.ProductOrderDetails]: {
      screen: ProductOrderDetails
    },
    [RoutesName.InfoPage]: {
      screen: InfoPage
    },
    [RoutesName.Enquiry]: {
      screen: Enquiry
    },
    [RoutesName.LookBook]: {
      screen: LookBook
    },
    [RoutesName.ListLookBook]: {
      screen: ListLookBook
    },
    [RoutesName.WebViewHome]: {
      screen: WebViewHome
    },
    [RoutesName.Location]: {
      screen: Location
    },
    [RoutesName.LocationDetail]: {
      screen: LocationDetail
    },
    [RoutesName.MapGuildUser]: {
      screen: MapGuildUser
    },
    [RoutesName.ScanBarcode]: {
      screen: ScanBarcode
    },
    [RoutesName.SearchProductScreen]: {
      screen: SearchProductScreen
    },
    [RoutesName.ListProductSearch]: {
      screen: ListProductSearch
    },
    [RoutesName.ListNotification]: {
      screen: ListNotification
    },
    [RoutesName.DetailNotification]: {
      screen: DetailNotification
    },
    [RoutesName.MapCurrentStore]: {
      screen: MapCurrentStore
    }
  },
  {
    headerMode: "screen",
    defaultNavigationOptions: {
      gesturesEnabled: false
    },
    transitionConfig: nav => handleCustomTransition(nav)
  }
);

MoreStack.navigationOptions = ({ navigation }) => {
  const indexRoute = navigation.state.index;
  const tabBarVisible = Utils.getSafeValue(
    navigation.state.routes[indexRoute],
    "params.isShowTabBar",
    true
  );

  return {
    tabBarVisible
  };
};

export const TabNav = createBottomTabNavigator(
  {
    [RoutesName.HomeStack]: {
      screen: HomeStack
    },
    [RoutesName.SearchStack]: {
      screen: SearchStack
    },
    [RoutesName.WatchListStack]: {
      screen: WatchListStack
    },
    [RoutesName.ShopBagStack]: {
      screen: ShopBagStack
    },
    [RoutesName.MoreStack]: {
      screen: MoreStack
    }
  },
  {
    headerMode: "none",
    tabBarComponent: BottomTabBar,
    backBehavior: "none"
  }
);

const AppStack = createStackNavigator(
  {
    [RoutesName.TabNav]: {
      screen: TabNav
    },
    [RoutesName.MagentoLogin]: {
      screen: MagentoLogin
    },
    [RoutesName.ForgetPassWord]: {
      screen: ForgetPassWord
    },
    [RoutesName.MagentoRegister]: {
      screen: MagentoRegister
    },
    [RoutesName.RegisterWebView]: {
      screen: RegisterWebView
    },
    [RoutesName.CheckOut]: {
      screen: CheckOut,
      navigationOptions: {
        gesturesEnabled: false
      }
    },
    [RoutesName.EditAddress]: {
      screen: NewAddress
    },
    [RoutesName.GeneralTerm]: {
      screen: GeneralTerm
    },
    [RoutesName.WebCheckOut]: {
      screen: WebCheckout
    },
    [RoutesName.ThankYouPage]: {
      screen: ThankYouPage
    },
    [RoutesName.SourceParamComponent]: {
      screen: SourceParamComponent
    }
  },
  {
    headerMode: "none",
    defaultNavigationOptions: {
      gesturesEnabled: false
    }
  }
);

const customTransitionRootStack = ({ scenes }) => {
  const prevScene = scenes[scenes.length - 2];
  const nextScene = scenes[scenes.length - 1];

  if (
    prevScene &&
    prevScene.route.routeName === RoutesName.SplashLoading &&
    nextScene.route.routeName === RoutesName.AppStack
  ) {
    return TransitionConfig.flipY();
  }

  return StackViewTransitionConfigs.defaultTransitionConfig;
};

//TODO: Create Switch navigator here to prevent android user can back to Splash screen.
const RootStack = createStackNavigator(
  {
    [RoutesName.SplashLoading]: {
      screen: SplashLoading
    },
    [RoutesName.AppStack]: {
      screen: AppStack
    }
  },
  {
    headerMode: "none",
    // transitionConfig: nav => customTransitionRootStack(nav),
    defaultNavigationOptions: {
      gesturesEnabled: false
    }
  }
);

const defaultGetStateForAction = RootStack.router.getStateForAction;
RootStack.router.getStateForAction = (action, state) => {
  if (!isIOS) {
    const root = state ? state.routes[state.index] : null;
    const tabNav = root && root.routes ? root.routes[root.index] : null;
    const stack = tabNav && tabNav.routes ? tabNav.routes[tabNav.index] : null;
    const indexStack = stack ? stack.index : 0;

    if (
      action.type === NavigationActions.BACK &&
      tabNav &&
      tabNav.routeName === RoutesName.TabNav &&
      stack &&
      indexStack === 0
    ) {
      // close application.
      return state;
    }

    return defaultGetStateForAction(action, state);
  }

  return defaultGetStateForAction(action, state);
};

const LoginStack = createStackNavigator(
  {
    [RoutesName.DevLogin]: {
      screen: DevLogin,
      navigationOptions: {
        header: null
      }
    },
    [RoutesName.ListApp]: {
      screen: ListApp
    },
    [RoutesName.ListLanguageSupport]: {
      screen: ListLanguageSupport
    }
  },
  {
    headerMode: "none",
    defaultNavigationOptions: {
      gesturesEnabled: false
    }
  }
);

const AppNavigator = createSwitchNavigator(
  {
    [RoutesName.LoginStack]: {
      screen: LoginStack
    },
    [RoutesName.RootStack]: {
      screen: RootStack
    }
  },
  {
    // headerMode: "none"
    initialRouteName: RoutesName.LoginStack
  }
);

export const createAppNav = (initRoute = RoutesName.LoginStack) => {
  return createAppContainer(
    createSwitchNavigator(
      {
        [RoutesName.LoginStack]: {
          screen: LoginStack
        },
        [RoutesName.RootStack]: {
          screen: RootStack
        }
      },
      {
        initialRouteName: initRoute
      }
    )
  );
};

export default createAppContainer(AppNavigator);
