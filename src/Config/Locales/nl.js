export default {
  RootApp: {
    textReturnListApp: "Wilt u terugkeren naar de App lijst?",
    logOut: "Uitloggen",
    cancel: "Annuleer",
    textOK: "OK",
    returnAppList: "Om terug te keren naar de App lijst?",
    shakeYourPhone: "Schud de telefoon",
    retryText: "Probeer nogmaals.",
    signIn: "Inloggen",
    orSignInWidth: "Of log in met",
    continueText: "Voortzetten",
    doYouWantToLogOut: "Wilt u uitloggen?",
    facebookText: "Facebook",
    googleText: "Google",
    buildVersion: "Build version",
    appKeyText: "App Key"
  },
  AlertInfo: {
    textNo: "Nee",
    textYes: "Ja",
    retryConnect: "Probeer nogmaals.",
    textOK: "OK"
  },
  ErrMsg: {
    sysErr: "Het systeem loopt tegen een onbekende fout aan.",
    timeOutErr: "Server time-out",
    networkErr: "Netwerk Fout",
    titleErr: "Fout",
    noConnect: "Geen internetverbinding",
    plsTryAgain: "Probeer nogmaals.",
    cantFindWay: "Can't find a way here"
  },
  TypeSortOrder: {
    Newest_First: "Nieuwste eerst",
    Oldest_First: "Oudste eerst",
    Lowest_Price_First: "Laagste prijs eerst",
    Highest_Price_First: "Hoogste prijs eerst"
  },
  TypeFilterOrder: {
    Show_All: "Toon alle",
    Pending: "In afwachting",
    Cancelled: "Annuleer",
    Shipped: "Verzonden"
  },
  DayTime: {
    Monday: "Maandag",
    Tuesday: "Dinsdag",
    Wednesday: "Woensdag",
    Thursday: "Donderdag",
    Friday: "Vrijdag",
    Saturday: "Zaterdag",
    Sunday: "Zondag"
  },
  ListApp: {
    notHaveAppYet: "U heeft nog geen apps gemaakt.",
    pleaseGoToWeb: "Ga naar www.JMango360.com",
    toCreateApp: "om uw eerste app te maken!"
  },
  EcommerceCatalog: {
    filter: "Filter",
    view: "Bekijk",
    sort: "Sorteren op",
    asLowAs: "Vanaf",
    from: "Van",
    to: "tot",
    startAt: "Vanaf",
    outOfStock: "Niet op voorraad",
    showPrice: "Toon Prijs",
    doneTextBtn: "Klaar",
    showMore: "Toon meer",
    showLess: "Toon minder",
    sortToolBar: "Sorteren",
    noProducts: "Geen producten"
  },
  DrawerTypeList: {
    GridView: "Raster",
    ListView: "Lijst",
    SingleView: "Enkel"
  },
  DrawerSort: {
    lowToHigh: "Laag naar hoog",
    highToLow: "Hoog naar laag",
    ascending: "Oplopend",
    descending: "Aflopend"
  },
  DrawerFilter: {
    filterBy: "Filter door",
    clearAll: "Wis alle",
    shoppingBy: "U zoekt momenteel op"
  },
  SearchProduct: {
    placeHolder: "Zoeken",
    noRecentSearch: "Geen recente zoekopdrachten",
    noSearchResultFound: "Geen producten gevonden",
    clearAll: "Wis alle",
    searchHistory: "Geen recente zoekopdrachten",
    recentViewed: "Recent bekeken",
    matchingCate: "Corresponderende categorieën",
    noItemsFound: "Geen producten gevonden"
  },
  ProductDetail: {
    charLeft: "tekens over",
    newText: "Nieuw",
    defaultOptionTitle: "Kies een optie",
    donePicker: "Klaar",
    cancelPicker: "Annuleer",
    textQuantity: "Aantal",
    tierBuy: "Bestel {{quantity}} voor ",
    tierEach: " per stuk en ",
    tierPercent: " bespaar {{savePercentage}}%",
    placeHolderText: "Vul in",
    outOfStock: "Niet op voorraad",
    addToCartText: "In winkelwagen",
    addingText: "Toevoegen",
    addedText: "Toegevoegd",
    updateCartText: "Update winkelwagen",
    outOfStockText: "Sorry, dit product is niet op voorraad",
    additionInfoText: "Aanvullende informatie",
    reviewText: "Review",
    seeAllText: "Zie alles",
    addReviewText: "Voeg je review toe",
    beFirstReviewText: "Review dit product als eerste",
    writeReview: "Schrijf een review",
    relatedProduct: "Gerelateerde producten",
    upSellProduct: "Andere klanten bekeken ook",
    titleError: "Vul alle verplichte velden",
    headerDetail: "Details",
    showPriceText: "Toon Prijs",
    loginToWriteReview: "Login om een review te schrijven",
    titleAlertATC:
      "Voordat dit product in de winkelwagen toegevoegd kan worden, moeten bepaalde gegevens nog geselecteerd worden. Klik op 'Ja' om de product details te bekijken.",
    priceText: "Prijs",
    requireFieldErr: "is verplicht",
    groupProductErr:
      "Geef het aantal aan van tenminste __n van de producten, a.u.b",
    errorTitle: "Fout",
    itemAddedToWatchList: "Product toegevoegd aan verlanglijst",
    itemUpdatedWatchList: "Het product is bijgewerkt in je verlanglijst",
    textRequireField: "Verplicht veld",
    overviewCustomerReview: "Overzicht klantreviews",
    byText: "door",
    ofText: "van",
    reviewSingle: " review",
    reviewMultiple: " reviews",
    contentAlertATC: "Klik op 'Ja' om de product details te bekijken.",
    deliveryText: "Bezorging"
  },
  Reviews: {
    headerTitleReview: "Reviews",
    pleaseFillRequire: "Vul alle verplichte velden",
    howToRate: "Totaal oordeel van dit product",
    submitReview: "Verstuur",
    textOK: "OK",
    titleReivewAlert: "Review is geplaatst",
    cancelHeader: "Annuleer",
    requireField: "Verplicht veld",
    byText: "door",
    reviewed: "gereviewd"
  },
  Location: {
    noAddress: "Geen adres beschikbaar",
    meterAway: "weg",
    tabAddress: "Straat + huisnummer",
    tabMap: "Kaart",
    errGetLocation: "Kan de huidige locatie niet vinden",
    textGetDirection: "Open routebeschrijving",
    textTimeOpenStore: "OPENINGSTIJDEN",
    phone: "TELEFOON",
    email: "E-MAILADRES",
    url: "URL",
    titleTimeClosed: "Gesloten"
  },
  ScanBarcode: {
    enterProductCode: "Voer product code in",
    sendAlertBarcode: "VERSTUUR",
    closeAlertBarcode: "SLUIT",
    placeholderInput: "Barcode op het product",
    isGettingData: "Validating product code…", //not implement B2B yet.
    nullDataBarcode: "Geen product gevonden met deze barcode",
    titleConfigProduct:
      "Gelieve het product samenstellen voordat u het toevoegd aan de winkelwagen",
    textBtnConfigProduct: "Stel product samen",
    notFoundProduct: "Geen product gevonden met deze barcode",
    textOK: "OK"
  },
  MagentoRegister: {
    requireField: "Vul alle verplichte velden",
    register: "Registreren",
    textOK: "OK"
  },
  ShoppingCart: {
    textDelete: "Verwijder",
    discount: "Korting",
    addCoupon: "Voeg kortingscode toe",
    enterCounpon: "Vul hier je coupon code in",
    textCancel: "Annuleer",
    validating: "Controle kortingscode",
    crossSell: "Andere klanten bekeken ook",
    orderTotal: "Totaal orderbedrag",
    textCheckOut: "Betalen",
    emptyCart: " Je winkelwagen is leeg",
    subTotalItem: "Subtotaal ({{itemCount}} product)",
    subTotalItems: "Subtotaal ({{itemCount}} producten)",
    viewDetails: "Toon details",
    subTotal: "Subtotaal",
    invalidCoupon: "Ongeldige kortingscode",
    correctCouponCode: "Vul aub correcte kortingscode in",
    shippingFee: "Verzendkosten",
    tax: "BTW"
  },
  BottomTabBar: {
    Home: "Home",
    Search: "Zoek",
    Watchlist: "Verlanglijst",
    ShoppingCart: "Winkelwagen",
    More: "Meer"
  },
  FormInput: {
    errorRequired: "{{label}} is verplicht",
    invalidEmail: "E-mailadres onjuist"
  },
  CheckOut: {
    textNext: "Volgende",
    textPrevious: "Vorige",
    confirmPlaceOrder:
      "Ik begrijp dat ik dit product moet betalen en ik ga akkoord met de algemene voorwaarden",
    textPlaceOrder: "Verstuur Bestelling",
    stepBillingInfo: "Factuur",
    stepShippingAddress: "Bezorging",
    stepShippingMethod: "Verzending",
    stepPaymentMethod: "Betaling",
    stepOrderReview: "Overzicht",
    headerStepBillingInfo: "Factuurgegevens",
    headerStepShippingAddress: "Verzendgegevens",
    headerStepShippingMethod: "Verzendmethode",
    headerStepPaymentMethod: "Betaalmethode",
    headerStepOrderReview: "Overzicht Bestelling",
    textCancelCheckOut: "Annuleer checkout",
    confirmCancelCheckOut:
      "Wil je de checkout annuleren en naar de winkelwagen terugkeren?",
    textYes: "Ja",
    textNo: "Nee",
    confirmSavePaymentBraintree:
      "Betaalgegevens opslaan voor toekomstige transacties?",
    textEdit: "Bewerken",
    textGrandTotal: "Totaal",
    textShipping: "Verzendkosten",
    orderSummary: "Overzicht bestelling",
    textQty: "Aantal",
    itemPrice: "Prijs product",
    subTotal: "Subtotaal",
    selectPaymentMethod: "Selecteer een betaalmethode",
    selectShippingMethod: "Selecteer een verzendmethode",
    idealBank: "IDEAL BANK",
    requireField: "Vul alle verplichte velden",
    country: "Land",
    accountHolder: "Naam account eigenaar",
    submitPayment: "Bevestig betaling",
    errorSourceParam:
      "Je betaling is niet gelukt en je bestelling kan niet worden verwerkt.",
    continueShopping: "Verder winkelen",
    payAgain: "Betaal nogmaals",
    shipTothisAddress: "Verzend naar dit adres",
    shipTodifferent: "Verzend naar verschillende adressen",
    editAddressDetail: "Wijzig adres gegevens",
    selectBilling: "Selecteer een factuuradres",
    selectShipping: "Selecteer een verzendadres",
    sameAsBilling: "Hetzelfde als factuuradres",
    textFooterBilling: "Gebruik een andere factuuradres",
    textFooterShipping: "Verzend naar verschillende adressen",
    subTotalItem: "Subtotaal ({{itemCount}} Product)",
    subTotalItems: "Subtotaal ({{itemCount}} Producten)",
    errorCurrencyBraintree:
      "Deze transactie kan niet verwerkt worden omdat er gebruik wordt gemaakt van een valuta die wij niet ondersteunen. Probeer nogmaals met een andere betaalmethode."
  },
  Login: {
    textLogin: "Inloggen"
  },
  WishList: {
    outOfStock: "Niet op voorraad",
    qty: "Aantal",
    textPrice: "Prijs",
    textPriceConfig: "Prijs zoals samengesteld",
    textShowLess: "Toon minder",
    textShowMore: "Toon meer",
    textNote: "Notitie",
    textAddNote: "Voeg hier uw notitie toe",
    textEditNote: "Notitie bewerken",
    textCancel: "Annuleer",
    aboveNoitem: "Er zijn geen producten",
    belowNoitem: "in de verlanglijst",
    textDone: "Klaar",
    enterYourNote: "Voeg hier uw notitie toe",
    messageAddToCart:
      "Voordat dit product in de winkelwagen toegevoegd kan worden, moeten bepaalde gegevens nog geselecteerd worden. Klik op 'Ja' om de product details te bekijken."
  },
  WebCheckOut: {
    aboveMessage: "U verlaat de betaalpagina",
    belowMessage: "Verder winkelen"
  },
  ModalConfirm: {
    textYes: "Ja",
    textNo: "Nee"
  },
  ModalQuanity: {
    textQuantity: "Aantal",
    textAdd: "Voeg toe",
    textCancel: "Annuleer"
  },
  NewAddress: {
    requireField: "Vul alle verplichte velden",
    textBillerDetail: "Klantgegevens",
    textBillingAddress: "Factuurgegevens",
    textNext: "Volgende",
    textNo: "Nee",
    aboveModalConfirm: "Niet opgeslagen wijzigingen zullen verloren gaan.",
    belowModalConfirm: "Wilt u verdergaan?",
    textYes: "Ja",
    textDone: "Klaar",
    noAddress: "Geen adres aangemaakt",
    addNew: "Voeg nieuw adres toe",
    search: "Zoek",
    cancel: "Annuleer"
  },
  DevLogin: {
    loginToPreview: "Login en bekijk uw JMango360 apps",
    email: "E-mailadres",
    passWord: "Wachtwoord",
    enviroment: "Enviroment"
  },
  EditProfile: {
    saveChanges: "Wijzigingen opslaan",
    textOK: "OK",
    male: "#N/A",
    female: "#N/A"
  },
  ModalSaveChange: {
    aboveMessage: "Wijzigingen zijn aangebracht",
    belowMessage: "Wilt u alle wijzigingen opslaan?",
    save: "Ja, sla wijzigingen op",
    noSave: "Nee, sla wijzigingen niet op",
    textCancel: "Annuleer"
  },
  Enquiry: {
    send: "Verstuur",
    textOK: "OK",
    requireField: "Vul alle verplichte velden",
    submitMessage: "Het formulier is verzonden.",
    yourName: "Naam",
    phoneNumber: "Telefoonnummer",
    emailAddress: "E-mailadres",
    enquiryAbout: "Selecteer aanvraag",
    enterMessageHere: "Voer uw bericht hier in",
    message: "Bericht"
  },
  ForgetPassWord: {
    content:
      "Vertel ons je emailadres die je gebruikt hebt om in te loggen, dan zullen wij je een email versturen met de instructies",
    send: "Verstuur",
    textOK: "OK",
    emailAddress: "E-mailadres",
    pleaseEntervalidEmail: "Vul een geldig e-mailadres in.",
    messageSuccess: "Er is een link gestuurd om uw wachtwoord te wijzigen naar"
  },
  MagentoLogin: {
    guestCheckout: "Gast checkout",
    signIn: "Inloggen",
    createNow: "Creëer nu",
    dontHaveAccount: "Heb je nog geen account?",
    forgotPassWord: "Watchtwoord vergeten?",
    invalidAccount: "Ongeldige gebruikersnaam en/of wachtwoord",
    emailAddress: "E-mailadres",
    passWord: "Wachtwoord"
  },
  MyAccount: {
    logOut: "Uitloggen",
    editProfile: "Wijzig profiel",
    myOrder: "Mijn bestellingen",
    myWatchList: "Mijn verlanglijst",
    addressBook: "Adresboek",
    editAddressDetail: "Wijzig adres gegevens",
    buildText: "Build",
    doYouWantToLogOut: "Wilt u uitloggen?"
  },
  DraweSortOrder: {
    titleSort: "Filter by",
    resetAll: "Reset alles",
    sortBy: "Sorteren op",
    reset: "Reset",
    filterBy: "Filter door"
  },
  MyOrder: {
    noOrder: "Je hebt geen bestellingen"
  },
  OrderDetails: {
    status: "Status",
    order: "Bestelling #",
    orderDate: "Orderdatum",
    totalCost: "Totaal",
    price: "Prijs",
    sku: "SKU",
    viewDetails: "Toon details",
    shippingDetails: "Verzendgegevens",
    billingDetails: "Factuurgegevens",
    shippingMethod: "Verzendmethode",
    paymentMethod: "Betaalmethode",
    orderSummary: "Overzicht bestelling ({{itemCount}} Product)",
    orderSummarys: "Overzicht bestelling ({{itemCount}} Producten)"
  },
  ProductOrderDetails: {
    quantity: "Aantal",
    price: "Prijs",
    subtotal: "Subtotaal",
    bundleContains: "Bundel bevat"
  },
  ThankYouPage: {
    content: "Bedankt voor het winkelen bij ons",
    orderNumber: "Uw order nummer is",
    infoContent:
      "U ontvangt een orderbevestigingsemail met de details van uw bestelling",
    viewOrder: "Bestelling bekijken",
    backToHome: "Terug naar home"
  },
  Header: {
    headerBillingAddress: "Factuurgegevens",
    headerShippingAddress: "Verzendgegevens",
    headerEditProfile: "Wijzig profiel",
    headerForgetPass: "Wachtwoord vergeten?",
    headerRegister: "Registreer hier",
    headerMyOrder: "Mijn bestellingen",
    headerOrderDetails: "Bestelgegevens",
    headerShoppingCart: "Mijn winkelwagen",
    headerEditAddress: "Adres wijzigen",
    headerNewAddress: "Nieuw Adres",
    headerAddressBook: "Adresboek",
    headerWishList: "Mijn verlanglijst"
  },
  PushNotifi: {
    viewMore: "Meer bekijken",
    noNotifi: "Geen notificatie",
    goToText: "Ga naar",
    headerNotification: "Notificatie",
    headerDetail: "Details",
    warningMsg:
      "Het openen van deze bericht zal je huidige activiteit beëindigen"
  },
  FormAddress: {
    firstName: "Voornaam",
    lastName: "Achternaam",
    company: "Bedrijf",
    address: "Straat + huisnummer",
    city: "Plaatsnaam",
    state: "Staat",
    zipcode: "Postcode",
    phoneNumber: "Telefoonnummer",
    optional: "optioneel",
    pleaseFillall: "Vul alle verplichte velden",
    isrequired: "is verplicht",
    email: "E-mailadres",
    country: "Land"
  },
  HeaderMore: {
    logIn: "Inloggen",
    viewYourProfile: "Bekijk je profiel"
  },
  HomeScreen: {
    titleError: "FOUT",
    contentError: "Geen producten gevonden"
  }
};
