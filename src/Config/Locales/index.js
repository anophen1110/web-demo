import en from "./en";
import vn from "./vn";
import nl from "./nl";

export default { vn, en, nl };
