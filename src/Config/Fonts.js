import { Platform } from "react-native";
const isIOS = Platform.OS === "ios";

export const Fonts = {
  OpenSansBold: "OpenSans-Bold",
  OpenSansBoldItalic: "OpenSans-BoldItalic",
  OpenSansExtraBold: "OpenSans-ExtraBold",
  OpenSansExtraBoldItalic: "OpenSans-ExtraBoldItalic",
  OpenSansItalic: "OpenSans-Italic",
  OpenSansLight: "OpenSans-Light",
  OpenSansLightItalic: "OpenSans-LightItalic",
  OpenSansRegular: isIOS ? "OpenSans" : "OpenSans-Regular",
  OpenSansSemiBold: isIOS ? "OpenSans-SemiBold" : "semiBold", //fix font name on android assets.
  OpenSansSemiBoldItalic: "OpenSans-SemiBoldItalic"
};

export default Fonts;
