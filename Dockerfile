FROM node:alpine
WORKDIR /web-demo
COPY package.json /web-demo
RUN npm install --only=prod
COPY src /web-demo/src
COPY public /web-demo/public
EXPOSE 3000
CMD npm start
